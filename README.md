# dev-doc

[小点](./docs/a-一些问题.md)



配置文档

- [Spring Initializr](https://start.spring.io/)
- [maven-3 各版本](http://archive.apache.org/dist/maven/maven-3/)
- [maven_all_version](http://archive.apache.org/dist/maven/)
- [jetbrains_other_version](https://www.jetbrains.com/idea/download/other.html)
- [hadoop_all_version](http://archive.apache.org/dist/hadoop/core/)
- [hadoop_other_version](http://mirror.bit.edu.cn/apache/hadoop/common/)
- [spark_other_version](https://archive.apache.org/dist/spark/)
- [flink_all_version](http://archive.apache.org/dist/flink/)
- [zookeeper_other_version](https://mirrors.tuna.tsinghua.edu.cn/apache/zookeeper/)
- [kafka_all_version](http://archive.apache.org/dist/kafka/)

## 大数据开发

- [hive 元数据表理解](https://blog.csdn.net/xjp8587/article/details/81411879)

- [grafana 模板](https://grafana.com/grafana/dashboards/8919)

- [Maven Wrapper](https://www.liaoxuefeng.com/wiki/1252599548343744/1305148057976866)

- [hudi 中文教程](https://github.com/apachecn/hudi-doc-zh/)

> **Flink**

- [Flink 中文视频课程](https://github.com/flink-china/flink-training-course)

- [Flink 从入门到精通](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzIxMTE0ODU5NQ==&action=getalbum&album_id=1337172142412169216&scene=173&from_msgid=2650237495&from_itemidx=1&count=3#wechat_redirect)

- [深入解读 Flink SQL 1.13](https://mp.weixin.qq.com/s/KaWJ99oGn3WJysfc5OcmTA)




## Java

- [IDEA建立Spring MVC Hello World 详细入门教程](https://www.cnblogs.com/wormday/p/8435617.html)

- [快速了解 Java 9 - 16 新特性](https://mp.weixin.qq.com/s/U8fSyDd3T8_A_SW77KF7lw)

## 机器学习

- [Machine-Learning-Notes](https://github.com/Sophia-11/Machine-Learning-Notes/blob/master/README.md)



## 云原生

- [Kubernetes中文社区 | 中文文档](http://docs.kubernetes.org.cn/)

- https://youdianzhishi.com/web
- [容器服务Kubernetes版](https://help.aliyun.com/product/85222.html)

## 网络相关

- 抓包： https://www.wireshark.org/download.html





![docker 架构图](./docs/picture/docker.webp)

#### 闲了看一看

https://www.junmajinlong.com/

https://veal98.gitee.io/cs-wiki/#/README

https://help.aliyun.com/document_detail/212405.html?spm=a2c4g.11186623.6.669.5ec63ad2acZLWl

https://help.aliyun.com/document_detail/86742.html?spm=a2c4g.11186623.6.643.f0be4bdbXoEzis

https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzIxMTE0ODU5NQ==&action=getalbum&album_id=1337172142412169216&scene=173&from_msgid=2650239562&from_itemidx=1&count=3&nolastread=1#wechat_redirect