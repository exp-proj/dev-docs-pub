#### Docker 高级网络配置

> 本章将介绍 Docker 的一些高级网络配置和选项。 

- 当 Docker 启动时，会自动在主机上创建一个 docker0 虚拟网桥，实际上是 Linux 的一个 bridge，可以理解为一个软件交换机。它会在挂载到它的网口之间进 行转发。 

- 同时，Docker 随机分配一个本地未占用的私有网段（在 RFC1918 中定义）中的一 个地址给 docker0 接口。比如典型的 172.17.42.1 ，掩码为 255.255.0.0 。此后启动的容器内的网口也会自动分配一个同一网段 （ 172.17.0.0/16 ）的地址。 

- 当创建一个 Docker 容器的时候，同时会创建了一对 veth pair 接口（当数据包 发送到一个接口时，另外一个接口也可以收到相同的数据包）。这对接口一端在容 器内，即 eth0 ；另一端在本地并被挂载到 docker0 网桥，名称以 veth 开 头（例如 vethAQI2QT ）。通过这种方式，主机可以跟容器通信，容器之间也可 以相互通信。Docker 就创建了在主机和所有容器之间一个虚拟共享网络。



- 接下来的部分将介绍在一些场景中，Docker 所有的网络定制配置。以及通过 Linux 命令来调整、补充、甚至替换 Docker 默认的网络配置。

##### 快速配置指南 

下面是一个跟 Docker 网络相关的命令列表。 

其中有些命令选项只有在 Docker 服务启动的时候才能配置，而且不能马上生效。 

- `-b BRIDGE` or `--bridge=BRIDGE` --指定容器挂载的网桥 

- `--bip=CIDR` --定制 docker0 的掩码

- `-H SOCKET...` or `--host=SOCKET...` --Docker 服务端接收命令的通道 

- `--icc=true|false` --是否支持容器之间进行通信 

- `--ip-forward=true|false` --请看下文容器之间的通信 

- `--iptables=true|false` --是否允许 Docker 添加 iptables 规则 

- `--mtu=BYTES` --容器网络中的 MTU 

下面2个命令选项既可以在启动服务时指定，也可以 Docker 容器启动（ `docker run` ）时候指定。在 Docker 服务启动的时候指定则会成为默认值，后面执行 `docker run` 时可以覆盖设置的默认值。

- `--dns=IP_ADDRESS...` --使用指定的DNS服务器 

- `--dns-search=DOMAIN...` --指定DNS搜索域

最后这些选项只有在 docker run 执行时使用，因为它是针对容器的特性内容。

- `-h HOSTNAME` or `--hostname=HOSTNAME` --配置容器主机名 
- `--link=CONTAINER_NAME:ALIAS` --添加到另一个容器的连接 
- `--net=bridge|none|container:NAME_or_ID|host` --配置容器的桥接模式 
- `-p SPEC` or `--publish=SPEC` --映射容器端口到宿主主机 
- `-P` or `--publish-all=true|false` --映射容器所有端口到宿主主机



