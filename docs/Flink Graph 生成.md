Flink Graph 生成

第⼀层：Program -> StreamGraph

第⼆层：StreamGraph -> JobGraph

第三层：JobGraph -> ExecutionGraph

第四层：Execution -> 物理执⾏计划

![image-20210823160836745](F:\workspace\vova-dev-docs\share\pic\image-20210823160836745.png)

- 第一层 StreamGraph 从 Source 节点开始，每一次 transform 生成一个 StreamNode，两个 StreamNode 通过 StreamEdge 连接在一起,形成 StreamNode 和 StreamEdge 构成的DAG。
- 第二层 JobGraph，依旧从 Source 节点开始，然后去遍历寻找能够嵌到一起的 operator，如果能够嵌到一起则嵌到一起，不能嵌到一起的单独生成 jobVertex，通过 JobEdge 链接上下游 JobVertex，最终形成 JobVertex 层面的 DAG。
- JobVertex DAG 提交到任务以后，从 Source 节点开始排序,根据 JobVertex 生成ExecutionJobVertex，根据 jobVertex的IntermediateDataSet 构建IntermediateResult，然后 IntermediateResult 构建上下游的依赖关系，形成 ExecutionJobVertex 层面的 DAG 即 ExecutionGraph。
- 最后通过 ExecutionGraph 层到物理执行层。

### StreamGraph

在编写 Flink 的程序的时候，核心的要点是构造出数据处理的拓扑结构，即任务执行逻辑的 DAG。我们先来看一下 Flink 任务的拓扑在逻辑上是怎么保存的。

- 从 StreamExecutionEnvironment.execute 开始执⾏程序，将 transform 添加 StreamExecutionEnvironment 的 transformations

- 调⽤ StreamGraphGenerator 的 generate ⽅法，遍历 transformations 构建 StreamNode 及StreamEage

- 通过 streamEdge 连接 StreamNode



#### StreamExecutionEnvironment

**`StreamExecutionEnvironment`** 是 Flink 在流模式下任务执行的上下文，也是我们编写 Flink 程序的入口。

根据具体的执行环境不同，`StreamExecutionEnvironment` 有不同的具体实现类，如 `LocalStreamEnvironment`, `RemoteStreamEnvironment` 等。

`StreamExecutionEnvironment` 也提供了用来配置默认并行度、Checkpointing 等机制的方法，这些配置主要都保存在 `ExecutionConfig` 和 `CheckpointConfig` 中。我们现在先只关注拓扑结构的产生。

通常一个 Flink 任务是按照下面的流程来编写处理逻辑的：

```java
env.addSource(XXX)
  .map(XXX)
  .filter(XXX)
  .addSink(XXX)
```

添加数据源后获得 `DataStream`, 之后通过不同的算子不停地在 `DataStream` 上实现转换过滤等逻辑，最终将结果输出到 `DataSink` 中。

在 StreamExecutionEnvironment 内部使用一个 `List<StreamTransformation<?>> transformations` 来保留生成 `DataStream` 的所有转换。

## StreamTransformation

`StreamTransformation` 代表了生成 `DataStream` 的操作，每一个 `DataStream` 的底层都有对应的一个 `StreamTransformation`。

在 `DataStream` 上面通过 `map` 等算子不断进行转换，就得到了由 `StreamTransformation` 构成的图。当需要执行的时候，底层的这个图就会被转换成 `StreamGraph`。

`StreamTransformation` 在运行时并不一定对应着一个物理转换操作，有一些操作只是逻辑层面上的，比如 split/select/partitioning 等。

每一个 `StreamTransformation` 都有一个关联的 Id，这个 Id 是全局递增的。除此以外，还有 uid, slotSharingGroup, parallelism 等信息。

`StreamTransformation` 有很多具体的子类，如`SourceTransformation`、 `OneInputStreamTransformation`、`TwoInputTransformation`、`SideOutputTransformation`、 `SinkTransformation` 等等，这些分别对应了 DataStream 上的不同转换操作。

由于 `StreamTransformation` 中通常保留了其前向的 `StreamTransformation`，即其输入，因此可以据此还原出 DAG 的拓扑结构。

```java
// OneInputTransformation
public OneInputTransformation(
			StreamTransformation<IN> input,
			String name,
			OneInputStreamOperator<IN, OUT> operator,
			TypeInformation<OUT> outputType,
			int parallelism) {
		super(name, outputType, parallelism);
		this.input = input;
		this.operator = operator;
	}

// TwoInputTransformation
public TwoInputTransformation(
			StreamTransformation<IN1> input1,
			StreamTransformation<IN2> input2,
			String name,
			TwoInputStreamOperator<IN1, IN2, OUT> operator,
			TypeInformation<OUT> outputType,
			int parallelism) {
		super(name, outputType, parallelism);
		this.input1 = input1;
		this.input2 = input2;
		this.operator = operator;
	}
```

## DataStream

一个 `DataStream` 就表征了由同一种类型元素构成的数据流。

通过对 `DataStream` 应用 map/filter 等操作，可以将一个 `DataStream` 转换为另一个 `DataStream`，这个转换的过程就是根据不同的操作生成不同的 `StreamTransformation`，并将其加入 `StreamExecutionEnvironment` 的 `transformations` 列表中。

例如：

```java
//构造 StreamTransformation
OneInputTransformation<T, R> resultTransform = new OneInputTransformation<>(
				this.transformation,
				operatorName,
				operator,
				outTypeInfo,
				environment.getParallelism());

@SuppressWarnings({ "unchecked", "rawtypes" })
SingleOutputStreamOperator<R> returnStream = new SingleOutputStreamOperator(environment, resultTransform);
//加入到 StreamExecutionEnvironment 的列表中
getExecutionEnvironment().addOperator(resultTransform);
```

`DataStream` 的子类包括 `SingleOutputStreamOperator`、 `DataStreamSource` `KeyedStream` 、`IterativeStream`, `SplitStream`(已弃用)。这里要吐槽一下 `SingleOutputStreamOperator` 的这个类的命名，太容易和 `StreamOperator` 混淆了。

除了 `DataStream` 及其子类以外，其它的表征数据流的类还有 `ConnectedStreams` (两个流连接在一起)、 `WindowedStream`、`AllWindowedStream` 。这些数据流之间的转换可以参考 Flink 的[官方文档](https://ci.apache.org/projects/flink/flink-docs-master/dev/stream/operators/)。

## StreamOperator

在操作 `DataStream` 的时候，比如 `DataStream#map` 等，会要求我们提供一个自定义的处理函数。那么这些信息时如何保存在 `StreamTransformation` 中的呢？这里就要引入一个新的接口 `StreamOperator`。

`StreamOperator` 定义了对一个具体的算子的生命周期的管理，包括：

```java
	//生命周期
	void setup(StreamTask<?, ?> containingTask, StreamConfig config, Output<StreamRecord<OUT>> output);

	void open() throws Exception;

	void close() throws Exception;

	@Override
	void dispose() throws Exception;

	//状态管理
	OperatorSnapshotFutures snapshotState(
		long checkpointId,
		long timestamp,
		CheckpointOptions checkpointOptions,
		CheckpointStreamFactory storageLocation) throws Exception;

	void initializeState() throws Exception;

  //其它方法暂时省略
```

`StreamOperator` 的两个子接口 `OneInputStreamOperator` 和 `TwoInputStreamOperator` 则提供了操作数据流中具体元素的方法，而 `AbstractUdfStreamOperator` 这个抽象子类则提供了自定义处理函数对应的算子的基本实现:

```java
//OneInputStreamOperator
void processElement(StreamRecord<IN> element) throws Exception;
void processWatermark(Watermark mark) throws Exception;
void processLatencyMarker(LatencyMarker latencyMarker) throws Exception;

//TwoInputStreamOperator
void processElement1(StreamRecord<IN1> element) throws Exception;
void processElement2(StreamRecord<IN2> element) throws Exception;

//AbstractUdfStreamOperator 接受一个用户自定义的处理函数
public AbstractUdfStreamOperator(F userFunction) {
	this.userFunction = requireNonNull(userFunction);
	checkUdfCheckpointingPreconditions();
}
```

至于具体到诸如 map/fliter 等操作对应的 StreamOperator，基本都是在 `AbstractUdfStreamOperator` 的基础上实现的。以 `StreamMap` 为例：

```java
public class StreamMap<IN, OUT>
	extends AbstractUdfStreamOperator<OUT, MapFunction<IN, OUT>>
	implements OneInputStreamOperator<IN, OUT> {

	private static final long serialVersionUID = 1L;

	public StreamMap(MapFunction<IN, OUT> mapper) {
		super(mapper);
		chainingStrategy = ChainingStrategy.ALWAYS;
	}

	@Override
	public void processElement(StreamRecord<IN> element) throws Exception {
		output.collect(element.replace(userFunction.map(element.getValue())));
	}
}
```

由此，通过 **DataStream –> StreamTransformation –> StreamOperator** 这样的依赖关系，就可以完成 DataStream 的转换，并且保留数据流和应用在流上的算子之间的关系。

## StreamGraph

`StreamGraphGenerator` 会基于 `StreamExecutionEnvironment` 的 `transformations` 列表来生成 `StreamGraph`。

在遍历 `List<StreamTransformation>` 生成 `StreamGraph` 的时候，会递归调用`StreamGraphGenerator#transform`方法。

对于每一个 `StreamTransformation`, 确保当前其上游已经完成转换。

`StreamTransformations` 被转换为 `StreamGraph` 中的节点 `StreamNode`，并为上下游节点添加边 `StreamEdge`。

**StreamNode**

描述operator的逻辑节点

关键成员变量

- slotSharingGroup

- jobVertexClass

- inEdges

- outEdges

- transformationUID

**StreamEdge**

描述两个operator逻辑的链接边

关键变量

- sourceVertex

- targetVertex

```java
Collection<Integer> transformedIds;
		if (transform instanceof OneInputTransformation<?, ?>) {
			transformedIds = transformOneInputTransform((OneInputTransformation<?, ?>) transform);
		} else if (transform instanceof TwoInputTransformation<?, ?, ?>) {
			transformedIds = transformTwoInputTransform((TwoInputTransformation<?, ?, ?>) transform);
		} else if (transform instanceof SourceTransformation<?>) {
			transformedIds = transformSource((SourceTransformation<?>) transform);
		} else if (transform instanceof SinkTransformation<?>) {
			transformedIds = transformSink((SinkTransformation<?>) transform);
		} else if (transform instanceof UnionTransformation<?>) {
			transformedIds = transformUnion((UnionTransformation<?>) transform);
		} else if (transform instanceof SplitTransformation<?>) {
			transformedIds = transformSplit((SplitTransformation<?>) transform);
		} else if (transform instanceof SelectTransformation<?>) {
			transformedIds = transformSelect((SelectTransformation<?>) transform);
		} else if (transform instanceof FeedbackTransformation<?>) {
			transformedIds = transformFeedback((FeedbackTransformation<?>) transform);
		} else if (transform instanceof CoFeedbackTransformation<?>) {
			transformedIds = transformCoFeedback((CoFeedbackTransformation<?>) transform);
		} else if (transform instanceof PartitionTransformation<?>) {
			transformedIds = transformPartition((PartitionTransformation<?>) transform);
		} else if (transform instanceof SideOutputTransformation<?>) {
			transformedIds = transformSideOutput((SideOutputTransformation<?>) transform);
		} else {
			throw new IllegalStateException("Unknown transformation: " + transform);
		}
```

对于不同类型的 `StreamTransformation`，分别调用对应的转换方法，以 最典型的 `transformOneInputTransform` 为例：

```java
private <IN, OUT> Collection<Integer> transformOneInputTransform(OneInputTransformation<IN, OUT> transform) {

		//首先确保上游节点完成转换
		Collection<Integer> inputIds = transform(transform.getInput());

		// the recursive call might have already transformed this
    // 由于是递归调用的，可能已经完成了转换
		if (alreadyTransformed.containsKey(transform)) {
			return alreadyTransformed.get(transform);
		}

		//确定资源共享组，用户如果没有指定，默认是default
		String slotSharingGroup = determineSlotSharingGroup(transform.getSlotSharingGroup(), inputIds);

		//向 StreamGraph 中添加 Operator, 这一步会生成对应的 StreamNode
		streamGraph.addOperator(transform.getId(),
				slotSharingGroup,
				transform.getCoLocationGroupKey(),
				transform.getOperator(),
				transform.getInputType(),
				transform.getOutputType(),
				transform.getName());

		if (transform.getStateKeySelector() != null) {
			TypeSerializer<?> keySerializer = transform.getStateKeyType().createSerializer(env.getConfig());
			streamGraph.setOneInputStateKey(transform.getId(), transform.getStateKeySelector(), keySerializer);
		}

		streamGraph.setParallelism(transform.getId(), transform.getParallelism());
		streamGraph.setMaxParallelism(transform.getId(), transform.getMaxParallelism());

		//依次连接到上游节点，创建 StreamEdge
		for (Integer inputId: inputIds) {
			streamGraph.addEdge(inputId, transform.getId(), 0);
		}

		return Collections.singleton(transform.getId());
	}
```

接着看一看 `StreamGraph` 中对应的添加节点和边的方法:

```java
protected StreamNode addNode(Integer vertexID,
		String slotSharingGroup,
		@Nullable String coLocationGroup,
		Class<? extends AbstractInvokable> vertexClass,
		StreamOperator<?> operatorObject,
		String operatorName) {

		if (streamNodes.containsKey(vertexID)) {
			throw new RuntimeException("Duplicate vertexID " + vertexID);
		}

		StreamNode vertex = new StreamNode(environment,
			vertexID,
			slotSharingGroup,
			coLocationGroup,
			operatorObject,
			operatorName,
			new ArrayList<OutputSelector<?>>(),
			vertexClass);

		//创建 StreamNode，这里保存了 StreamOperator 和 vertexClass 信息
		streamNodes.put(vertexID, vertex);

		return vertex;
	}
```

在 `StreamNode` 中，保存了对应的 `StreamOperator` (从 `StreamTransformation` 得到)，并且还引入了变量 jobVertexClass 来表示该节点在 TaskManager 中运行时的实际任务类型。

```java
private final Class<? extends AbstractInvokable> jobVertexClass;
```

`AbstractInvokable` 是所有可以在 TaskManager 中运行的任务的抽象基础类，包括流式任务和批任务。`StreamTask` 是所有流式任务的基础类，其具体的子类包括 `SourceStreamTask`, `OneInputStreamTask`, `TwoInputStreamTask` 等。

对于一些不包含物理转换操作的 `StreamTransformation`，如 Partitioning, split/select, union，并不会生成 StreamNode，而是生成一个带有特定属性的虚拟节点。当添加一条有虚拟节点指向下游节点的边时，会找到虚拟节点上游的物理节点，在两个物理节点之间添加边，并把虚拟转换操作的属性附着上去。

以 `PartitionTansformation` 为例， `PartitionTansformation` 是 `KeyedStream` 对应的转换：

```java
//StreamGraphGenerator#transformPartition
private <T> Collection<Integer> transformPartition(PartitionTransformation<T> partition) {
		StreamTransformation<T> input = partition.getInput();
		List<Integer> resultIds = new ArrayList<>();

		//递归地转换上游节点
		Collection<Integer> transformedIds = transform(input);

		for (Integer transformedId: transformedIds) {
			int virtualId = StreamTransformation.getNewNodeId();
			//添加虚拟的 Partition 节点
			streamGraph.addVirtualPartitionNode(transformedId, virtualId, partition.getPartitioner());
			resultIds.add(virtualId);
		}

		return resultIds;
	}

// StreamGraph#addVirtualPartitionNode
public void addVirtualPartitionNode(Integer originalId, Integer virtualId, StreamPartitioner<?> partitioner) {

		if (virtualPartitionNodes.containsKey(virtualId)) {
			throw new IllegalStateException("Already has virtual partition node with id " + virtualId);
		}

		//添加一个虚拟节点，后续添加边的时候会连接到实际的物理节点
		virtualPartitionNodes.put(virtualId,
				new Tuple2<Integer, StreamPartitioner<?>>(originalId, partitioner));
	}
```

前面提到，在每一个物理节点的转换上，会调用 `StreamGraph#addEdge` 在输入节点和当前节点之间建立边的连接：

```java
private void addEdgeInternal(Integer upStreamVertexID,
			Integer downStreamVertexID,
			int typeNumber,
			StreamPartitioner<?> partitioner,
			List<String> outputNames,
			OutputTag outputTag) {

		//先判断是不是虚拟节点上的边，如果是，则找到虚拟节点上游对应的物理节点
		//在两个物理节点之间添加边，并把对应的 StreamPartitioner,或者 OutputTag 等补充信息添加到StreamEdge中
		if (virtualSideOutputNodes.containsKey(upStreamVertexID)) {
			......
		} else if (virtualPartitionNodes.containsKey(upStreamVertexID)) {
			int virtualId = upStreamVertexID;
			upStreamVertexID = virtualPartitionNodes.get(virtualId).f0;
			if (partitioner == null) {
				partitioner = virtualPartitionNodes.get(virtualId).f1;
			}
			addEdgeInternal(upStreamVertexID, downStreamVertexID, typeNumber, partitioner, outputNames, outputTag);
		} else {

			//两个物理节点
			StreamNode upstreamNode = getStreamNode(upStreamVertexID);
			StreamNode downstreamNode = getStreamNode(downStreamVertexID);

			// If no partitioner was specified and the parallelism of upstream and downstream
			// operator matches use forward partitioning, use rebalance otherwise.
			if (partitioner == null && upstreamNode.getParallelism() == downstreamNode.getParallelism()) {
				partitioner = new ForwardPartitioner<Object>();
			} else if (partitioner == null) {
				partitioner = new RebalancePartitioner<Object>();
			}

			if (partitioner instanceof ForwardPartitioner) {
				if (upstreamNode.getParallelism() != downstreamNode.getParallelism()) {
					throw new UnsupportedOperationException("Forward partitioning does not allow " +
							"change of parallelism. Upstream operation: " + upstreamNode + " parallelism: " + upstreamNode.getParallelism() +
							", downstream operation: " + downstreamNode + " parallelism: " + downstreamNode.getParallelism() +
							" You must use another partitioning strategy, such as broadcast, rebalance, shuffle or global.");
				}
			}

			//创建 StreamEdge，保留了 StreamPartitioner 等属性
			StreamEdge edge = new StreamEdge(upstreamNode, downstreamNode, typeNumber, outputNames, partitioner, outputTag);

			//分别将StreamEdge添加到上游节点和下游节点
			getStreamNode(edge.getSourceId()).addOutEdge(edge);
			getStreamNode(edge.getTargetId()).addInEdge(edge);
		}
	}
```

StreamGraph 是 Flink 任务最接近用户逻辑的 DAG 表示. 从 DataStream API 到 StramGraph 的过程



### JobGraph 的生成

具体步骤:

- 设置调度模式，Eager所有节点⽴即启动

- ⼴度优先遍历StreamGraph，为每个streamNode⽣成byte数组类型的hash值 

- 从source节点开始递归寻找chain到⼀起的operator，不能chain到⼀起的节点单独⽣成jobVertex，能

够chaind到⼀起的,开始节点⽣成jobVertex,其他节点以序列化的形式写⼊到StreamConfig，然后

merge到CHAINED_TASK_CONFIG ，然后通过JobEdge链接上下游JobVertex

- 将每个JobVertex的⼊边(StreamEdge)序列化到该StreamConfig

- 根据group name为每个JobVertext指定SlotSharingGroup

- 配置checkpoint

- 将缓存⽂件存⽂件的配置添加到configuration中 

- 设置置ExecutionConfig

## JobVertex

在 StreamGraph 中，每一个算子（Operator） 对应了图中的一个节点（StreamNode）。StreamGraph 会被进一步优化，将多个符合条件的节点串联（Chain） 在一起形成一个节点，从而减少数据在不同节点之间流动所产生的序列化、反序列化、网络传输的开销。多个算子被 chain 在一起的形成的节点在 `JobGraph` 中对应的就是 `JobVertex`。

每个 `JobVertex` 中包含一个或多个 Operators。 `JobVertex` 的主要成员变量包括

```java
	/** The ID of the vertex. */
	private final JobVertexID id;

	/** The alternative IDs of the vertex. */
	private final ArrayList<JobVertexID> idAlternatives = new ArrayList<>();

	/** The IDs of all operators contained in this vertex. */
	private final ArrayList<OperatorID> operatorIDs = new ArrayList<>();

	/** The alternative IDs of all operators contained in this vertex. */
	private final ArrayList<OperatorID> operatorIdsAlternatives = new ArrayList<>();

	/** List of produced data sets, one per writer */
	private final ArrayList<IntermediateDataSet> results = new ArrayList<IntermediateDataSet>();

	/** List of edges with incoming data. One per Reader. */
	private final ArrayList<JobEdge> inputs = new ArrayList<JobEdge>();

	/** Number of subtasks to split this task into at runtime.*/
	private int parallelism = ExecutionConfig.PARALLELISM_DEFAULT;
```

其输入是 `JobEdge` 列表, 输出是 `IntermediateDataSet` 列表。

## JobEdge

在 `StramGraph` 中，`StreamNode` 之间是通过 `StreamEdge` 建立连接的。在 `JobEdge` 中，对应的是 `JobEdge`。

和 `StreamEdge` 中同时保留了源节点和目标节点 （sourceId 和 targetId）不同，在 `JobEdge` 中只有源节点的信息。由于 `JobVertex` 中保存了所有输入的 `JobEdge` 的信息，因而同样可以在两个节点之间建立连接。更确切地说，`JobEdge` 是和节点的输出结果相关联的，我们看下 `JobEdge` 主要的成员变量：

```java
	/** The vertex connected to this edge. */
	private final JobVertex target;

	/** The distribution pattern that should be used for this job edge. */
  // DistributionPattern 决定了在上游节点（生产者）的子任务和下游节点（消费者）之间的连接模式
	private final DistributionPattern distributionPattern;

	/** The data set at the source of the edge, may be null if the edge is not yet connected*/
	private IntermediateDataSet source;

	/** The id of the source intermediate data set */
	private IntermediateDataSetID sourceId;

	/** Optional name for the data shipping strategy (forward, partition hash, rebalance, ...),
	 * to be displayed in the JSON plan */
	private String shipStrategyName;
```

其中 `ResultPartitionType` 表示中间结果的类型，说起来有点抽象，我们看下属性就明白了：

```java
	/** Can the partition be consumed while being produced? */
	private final boolean isPipelined;

	/** Does the partition produce back pressure when not consumed? */
	private final boolean hasBackPressure;

	/** Does this partition use a limited number of (network) buffers? */
	private final boolean isBounded;
```

这个要结合 Flink 任务运行时的内存管理机制来看，在后面的文章再进行分析。目前在 Stream 模式下使用的类型是 `PIPELINED_BOUNDED(true, true, true)`，上面的三个属性都是 true。

#### StreamConfig

对于每一个 `StreamOperator`, 也就是 `StreamGraph` 中的每一个 `StreamGraph`, 在生成 `JobGraph` 的过程中 `StreamingJobGraphGenerator` 都会创建一个对应的 `StreamConfig`。

`StreamConfig` 中保存了这个算子（operator） 在运行是需要的所有配置信息，这些信息都是通过 key/value 的形式存储在 `Configuration` 中的。例如：

```java
	//保存StreamOperator信息
	public void setStreamOperator(StreamOperator<?> operator) {
		if (operator != null) {
			config.setClass(USER_FUNCTION, operator.getClass());

			try {
				InstantiationUtil.writeObjectToConfig(operator, this.config, SERIALIZEDUDF);
			} catch (IOException e) {
				throw new StreamTaskException("Cannot serialize operator object "
						+ operator.getClass() + ".", e);
			}
		}
	}


  public void setChainedOutputs(List<StreamEdge> chainedOutputs) {
		try {
			InstantiationUtil.writeObjectToConfig(chainedOutputs, this.config, CHAINED_OUTPUTS);
		} catch (IOException e) {
			throw new StreamTaskException("Cannot serialize chained outputs.", e);
		}
	}

  public void setNonChainedOutputs(List<StreamEdge> outputvertexIDs) {
		try {
			InstantiationUtil.writeObjectToConfig(outputvertexIDs, this.config, NONCHAINED_OUTPUTS);
		} catch (IOException e) {
			throw new StreamTaskException("Cannot serialize non chained outputs.", e);
		}
	}

  public void setInPhysicalEdges(List<StreamEdge> inEdges) {
		try {
			InstantiationUtil.writeObjectToConfig(inEdges, this.config, IN_STREAM_EDGES);
		} catch (IOException e) {
			throw new StreamTaskException("Cannot serialize inward edges.", e);
		}
	}

  //......
```

## 从 StreamGraph 到 JobGraph

从 `StreamGraph` 到 `JobGraph` 的转换入口在 `StreamingJobGraphGenerator` 中。

首先来看下 `StreamingJobGraphGenerator` 的成员变量和入口函数：

```java
	//id -> JobVertex 的对应关系
	private final Map<Integer, JobVertex> jobVertices;
	//已经构建的JobVertex的id集合
	private final Collection<Integer> builtVertices;
  //物理边集合（不包含chain内部的边）, 按创建顺序排序
  private List<StreamEdge> physicalEdgesInOrder;
  //保存 operataor chain 的信息，部署时用来构建 OperatorChain，startNodeId -> (currentNodeId -> StreamConfig)
  private Map<Integer, Map<Integer, StreamConfig>> chainedConfigs;
  //所有节点的配置信息，id -> StreamConfig
  private Map<Integer, StreamConfig> vertexConfigs;
  //保存每个节点的名字，id -> chainedName
  private Map<Integer, String> chainedNames;

  //用于计算hash值的算法
	private final StreamGraphHasher defaultStreamGraphHasher;
	private final List<StreamGraphHasher> legacyStreamGraphHashers;
	//.....

private JobGraph createJobGraph() {

		// 调度模式，立即启动
		jobGraph.setScheduleMode(ScheduleMode.EAGER);

		// 广度优先遍历 StreamGraph 并且为每个SteamNode生成hash，hash值将被用于 JobVertexId 中
		// 保证如果提交的拓扑没有改变，则每次生成的hash都是一样的
		Map<Integer, byte[]> hashes = defaultStreamGraphHasher.traverseStreamGraphAndGenerateHashes(streamGraph);
		List<Map<Integer, byte[]>> legacyHashes = new ArrayList<>(legacyStreamGraphHashers.size());
		for (StreamGraphHasher hasher : legacyStreamGraphHashers) {
			legacyHashes.add(hasher.traverseStreamGraphAndGenerateHashes(streamGraph));
		}

		Map<Integer, List<Tuple2<byte[], byte[]>>> chainedOperatorHashes = new HashMap<>();

		// 主要的转换逻辑，生成 JobVetex， JobEdge 等
		setChaining(hashes, legacyHashes, chainedOperatorHashes);

		// 将每个JobVertex的输入边集合也序列化到该JobVertex的StreamConfig中
    // (出边集合已经在setChaining的时候写入了)
		setPhysicalEdges();

		// 根据group name，为每个 JobVertex 指定所属的 SlotSharingGroup
		// 以及针对 Iteration的头尾设置  CoLocationGroup
		setSlotSharingAndCoLocation();

		// 配置 checkpoint
		configureCheckpointing();

		// 添加用户提供的自定义的文件信息
		JobGraphGenerator.addUserArtifactEntries(streamGraph.getEnvironment().getCachedFiles(), jobGraph);

		// 将 StreamGraph 的 ExecutionConfig 序列化到 JobGraph 的配置中
		try {
			jobGraph.setExecutionConfig(streamGraph.getExecutionConfig());
		}
		catch (IOException e) {
			throw new IllegalConfigurationException("Could not serialize the ExecutionConfig." +
					"This indicates that non-serializable types (like custom serializers) were registered");
		}

		return jobGraph;
	}
```

`StreamingJobGraphGenerator#createJobGraph` 函数的逻辑也很清晰，首先为所有节点生成一个唯一的hash id，如果节点在多次提交中没有改变（包括并发度、上下游等），那么这个id就不会改变，这主要用于故障恢复。这里我们不能用 `StreamNode.id` 来代替，因为这是一个从 1 开始的静态计数变量，同样的 Job 可能会得到不一样的 id。然后就是最关键的 chaining 处理，和生成JobVetex、JobEdge等。之后就是写入各种配置相关的信息。

我们先来看一下，Flink 是如何确定两个 Operator 是否能够被 chain 到同一个节点的：

```java
//StreamEdge 两端的节点是否能够被 chain 到同一个 JobVertex 中
	public static boolean isChainable(StreamEdge edge, StreamGraph streamGraph) {
		//获取到上游和下游节点
		StreamNode upStreamVertex = streamGraph.getSourceVertex(edge);
		StreamNode downStreamVertex = streamGraph.getTargetVertex(edge);

		//获取到上游和下游节点具体的算子 StreamOperator
		StreamOperator<?> headOperator = upStreamVertex.getOperator();
		StreamOperator<?> outOperator = downStreamVertex.getOperator();

		return downStreamVertex.getInEdges().size() == 1 //下游节点只有一个输入
				&& outOperator != null
				&& headOperator != null
				&& upStreamVertex.isSameSlotSharingGroup(downStreamVertex) //在同一个slot共享组中
				//上下游算子的 chainning 策略，要允许chainning
				//默认的是 ALWAYS
				//在添加算子时，也可以强制使用 disableChain 设置为 NEVER
				&& outOperator.getChainingStrategy() == ChainingStrategy.ALWAYS
				&& (headOperator.getChainingStrategy() == ChainingStrategy.HEAD ||
					headOperator.getChainingStrategy() == ChainingStrategy.ALWAYS)
				//上下游节点之间的数据传输方式必须是FORWARD，而不能是REBALANCE等其它模式
				&& (edge.getPartitioner() instanceof ForwardPartitioner)
				//上下游节点的并行度要一致
				&& upStreamVertex.getParallelism() == downStreamVertex.getParallelism()
				&& streamGraph.isChainingEnabled();
	}
```

只要一条边两端的节点满足上面的条件，那么这两个节点就可以被串联在同一个 `JobVertex` 中。接着来就来看最为关键的函数 setChaining 的逻辑：

```java
	/**
	 * Sets up task chains from the source {@link StreamNode} instances.
	 *
	 * <p>This will recursively create all {@link JobVertex} instances.
	 */
	private void setChaining(Map<Integer, byte[]> hashes, List<Map<Integer, byte[]>> legacyHashes, Map<Integer, List<Tuple2<byte[], byte[]>>> chainedOperatorHashes) {
		for (Integer sourceNodeId : streamGraph.getSourceIDs()) {
			createChain(sourceNodeId, sourceNodeId, hashes, legacyHashes, 0, chainedOperatorHashes);
		}
	}


	//构建 operator chain（可能包含一个或多个StreamNode），返回值是当前的这个 operator chain 实际的输出边（不包括内部的边）
	//如果 currentNodeId != startNodeId, 说明当前节点在  operator chain 的内部
	private List<StreamEdge> createChain(
			Integer startNodeId,
			Integer currentNodeId,
			Map<Integer, byte[]> hashes,
			List<Map<Integer, byte[]>> legacyHashes,
			int chainIndex,
			Map<Integer, List<Tuple2<byte[], byte[]>>> chainedOperatorHashes) {

		if (!builtVertices.contains(startNodeId)) {

			//当前 operator chain 最终的输出边，不包括内部的边
			List<StreamEdge> transitiveOutEdges = new ArrayList<StreamEdge>();

			List<StreamEdge> chainableOutputs = new ArrayList<StreamEdge>();
			List<StreamEdge> nonChainableOutputs = new ArrayList<StreamEdge>();

			//将当前节点的出边分为两组，即 chainable 和 nonChainable
			for (StreamEdge outEdge : streamGraph.getStreamNode(currentNodeId).getOutEdges()) {
				if (isChainable(outEdge, streamGraph)) { //判断当前 StreamEdge 的上下游是否可以串联在一起
					chainableOutputs.add(outEdge);
				} else {
					nonChainableOutputs.add(outEdge);
				}
			}

			//对于chainable的输出边，递归调用，找到最终的输出边并加入到输出列表中
			for (StreamEdge chainable : chainableOutputs) {
				transitiveOutEdges.addAll(
						createChain(startNodeId, chainable.getTargetId(), hashes, legacyHashes, chainIndex + 1, chainedOperatorHashes));
			}

			//对于 nonChainable 的边
			for (StreamEdge nonChainable : nonChainableOutputs) {
				//这个边本身就应该加入到当前节点的输出列表中
				transitiveOutEdges.add(nonChainable);
				//递归调用，以下游节点为起点创建新的operator chain
				createChain(nonChainable.getTargetId(), nonChainable.getTargetId(), hashes, legacyHashes, 0, chainedOperatorHashes);
			}

			//用于保存一个operator chain所有 operator 的 hash 信息
			List<Tuple2<byte[], byte[]>> operatorHashes =
				chainedOperatorHashes.computeIfAbsent(startNodeId, k -> new ArrayList<>());

			byte[] primaryHashBytes = hashes.get(currentNodeId);

			for (Map<Integer, byte[]> legacyHash : legacyHashes) {
				operatorHashes.add(new Tuple2<>(primaryHashBytes, legacyHash.get(currentNodeId)));
			}

			//当前节点的名称，资源要求等信息
			chainedNames.put(currentNodeId, createChainedName(currentNodeId, chainableOutputs));
			chainedMinResources.put(currentNodeId, createChainedMinResources(currentNodeId, chainableOutputs));
			chainedPreferredResources.put(currentNodeId, createChainedPreferredResources(currentNodeId, chainableOutputs));

			//如果当前节点是起始节点, 则直接创建 JobVertex 并返回 StreamConfig, 否则先创建一个空的 StreamConfig
			//createJobVertex 函数就是根据 StreamNode 创建对应的 JobVertex, 并返回了空的 StreamConfig
			StreamConfig config = currentNodeId.equals(startNodeId)
					? createJobVertex(startNodeId, hashes, legacyHashes, chainedOperatorHashes)
					: new StreamConfig(new Configuration());

			// 设置 JobVertex 的 StreamConfig, 基本上是序列化 StreamNode 中的配置到 StreamConfig 中.
			// 其中包括 序列化器, StreamOperator, Checkpoint 等相关配置
			setVertexConfig(currentNodeId, config, chainableOutputs, nonChainableOutputs);

			if (currentNodeId.equals(startNodeId)) {
				// 如果是chain的起始节点。（不是chain中的节点，也会被标记成 chain start）
				config.setChainStart();
				config.setChainIndex(0);
				config.setOperatorName(streamGraph.getStreamNode(currentNodeId).getOperatorName());
				//把实际的输出边写入配置, 部署时会用到
				config.setOutEdgesInOrder(transitiveOutEdges);
				//operator chain 的头部 operator 的输出边，包括内部的边
				config.setOutEdges(streamGraph.getStreamNode(currentNodeId).getOutEdges());

				// 将当前节点(headOfChain)与所有出边相连
				for (StreamEdge edge : transitiveOutEdges) {
					// 通过StreamEdge构建出JobEdge，创建IntermediateDataSet，用来将JobVertex和JobEdge相连
					connect(startNodeId, edge);
				}

				// 将operator chain中所有子节点的 StreamConfig 写入到 headOfChain 节点的 CHAINED_TASK_CONFIG 配置中
				config.setTransitiveChainedTaskConfigs(chainedConfigs.get(startNodeId));

			} else {
				//如果是 operator chain 内部的节点
				Map<Integer, StreamConfig> chainedConfs = chainedConfigs.get(startNodeId);

				if (chainedConfs == null) {
					chainedConfigs.put(startNodeId, new HashMap<Integer, StreamConfig>());
				}
				config.setChainIndex(chainIndex);
				StreamNode node = streamGraph.getStreamNode(currentNodeId);
				config.setOperatorName(node.getOperatorName());
				// 将当前节点的 StreamConfig 添加所在的 operator chain 的 config 集合中
				chainedConfigs.get(startNodeId).put(currentNodeId, config);
			}

			//设置当前 operator 的 OperatorID
			config.setOperatorID(new OperatorID(primaryHashBytes));

			if (chainableOutputs.isEmpty()) {
				config.setChainEnd();
			}
			return transitiveOutEdges;

		} else {
			return new ArrayList<>();
		}
	}
```

上述过程实际上就是通过 DFS 遍历所有的 `StreamNode`, 并按照 chainable 的条件不停地将可以串联的呃 operator 放在同一个的 operator chain 中。每一个 `StreamNode` 的配置信息都会被序列化到对应的 `StreamConfig` 中。只有 operator chain 的头部节点会生成对应的 `JobVertex` ，一个 operator chain 的所有内部节点都会以序列化的形式写入头部节点的 `CHAINED_TASK_CONFIG` 配置项中。

每一个 operator chain 都会为所有的实际输出边创建对应的 `JobEdge`，并和 `JobVertex` 连接：

```java
private void connect(Integer headOfChain, StreamEdge edge) {

		physicalEdgesInOrder.add(edge);

		Integer downStreamvertexID = edge.getTargetId();

		//上下游节点
		JobVertex headVertex = jobVertices.get(headOfChain);
		JobVertex downStreamVertex = jobVertices.get(downStreamvertexID);

		StreamConfig downStreamConfig = new StreamConfig(downStreamVertex.getConfiguration());
		//下游节点增加一个输入
		downStreamConfig.setNumberOfInputs(downStreamConfig.getNumberOfInputs() + 1);

		StreamPartitioner<?> partitioner = edge.getPartitioner();
		JobEdge jobEdge;
		//创建 JobEdge 和 IntermediateDataSet
		//根据StreamPartitioner类型决定在上游节点（生产者）的子任务和下游节点（消费者）之间的连接模式
		if (partitioner instanceof ForwardPartitioner || partitioner instanceof RescalePartitioner) {
			jobEdge = downStreamVertex.connectNewDataSetAsInput(
				headVertex,
				DistributionPattern.POINTWISE,
				ResultPartitionType.PIPELINED_BOUNDED);
		} else {
			jobEdge = downStreamVertex.connectNewDataSetAsInput(
					headVertex,
					DistributionPattern.ALL_TO_ALL,
					ResultPartitionType.PIPELINED_BOUNDED);
		}
		// set strategy name so that web interface can show it.
		jobEdge.setShipStrategyName(partitioner.toString());

		if (LOG.isDebugEnabled()) {
			LOG.debug("CONNECTED: {} - {} -> {}", partitioner.getClass().getSimpleName(),
					headOfChain, downStreamvertexID);
		}
	}
```

> `JobGraph` 的关键在于将多个 `StreamNode` 优化为一个 `JobVertex`, 对应的 `StreamEdge` 则转化为 `JobEdge`, 并且 `JobVertex` 和 `JobEdge` 之间通过 `IntermediateDataSet` 形成一个生产者和消费者的连接关系。

`StreamGraph`, `JobGraph` 的生成过程，这两个执行图都是在 client 端生成的

### ExecutionGraph 的生成

Flink Job 运行时调度层核心的执行图 - `ExecutionGraph`

和 `StreamGraph` 以及 `JobGraph` 不同的是，`ExecutionGraph` 是在 JobManager 中生成的。 Client 向 JobManager 提交 `JobGraph` 后， JobManager 就会根据 `JobGraph` 来创建对应的 `ExecutionGraph`,并以此来调度任务。

### ExecutionJobVertex

在 `ExecutionGraph` 中，节点对应的类是 `ExecutionJobVertex`，与之对应的就是 `JobGraph` 中的 `JobVertex`。每一个 `ExexutionJobVertex` 都是由一个 `JobVertex` 生成的。

```java
	private final JobVertex jobVertex;

	private final List<OperatorID> operatorIDs;
	private final List<OperatorID> userDefinedOperatorIds;

	//ExecutionVertex 对应一个并行的子任务
	private final ExecutionVertex[] taskVertices;

	private final IntermediateResult[] producedDataSets;

	private final List<IntermediateResult> inputs;

	private final int parallelism;

	private final SlotSharingGroup slotSharingGroup;

	private final CoLocationGroup coLocationGroup;

	private final InputSplit[] inputSplits;

	private int maxParallelism;
```

### ExecutionVertex

`ExexutionJobVertex` 的成员变量中包含一个 `ExecutionVertex` 数组。我们知道，Flink Job 是可以指定任务的并行度的，在实际运行时，会有多个并行的任务同时在执行，对应到这里就是 `ExecutionVertex`。`ExecutionVertex` 是并行任务的一个子任务，算子的并行度是多少，那么就会有多少个 `ExecutionVertex`。

```java
	private final ExecutionJobVertex jobVertex;

	private final Map<IntermediateResultPartitionID, IntermediateResultPartition> resultPartitions;

	private final ExecutionEdge[][] inputEdges;

	private final int subTaskIndex;

	private final EvictingBoundedList<ArchivedExecution> priorExecutions;

	private volatile CoLocationConstraint locationConstraint;

	/** The current or latest execution attempt of this vertex's task. */
	private volatile Execution currentExecution;	// this field must never be null
```

### Execution

`Execution` 是对 `ExecutionVertex` 的一次执行，通过 `ExecutionAttemptId` 来唯一标识。

### IntermediateResult

在 `JobGraph` 中用 `IntermediateDataSet` 表示 `JobVertex` 的对外输出，一个 `JobGraph` 可能有 n(n >=0) 个输出。在 `ExecutionGraph` 中，与此对应的就是 `IntermediateResult`。

```java
	//对应的IntermediateDataSet的ID
	private final IntermediateDataSetID id;
	//生产者
	private final ExecutionJobVertex producer;

	//对应ExecutionJobVertex的并行度
	private final int numParallelProducers;

	private final IntermediateResultPartition[] partitions = new IntermediateResultPartition[numParallelProducers];

	private final ResultPartitionType resultType;
```

由于 `ExecutionJobVertex` 有 numParallelProducers 个并行的子任务，自然对应的每一个 `IntermediateResult` 就有 numParallelProducers 个生产者，每个生产者的在相应的 `IntermediateResult` 上的输出对应一个 `IntermediateResultPartition`。`IntermediateResultPartition` 表示的是 `ExecutionVertex` 的一个输出分区，即：

```
ExecutionJobVertex -->  IntermediateResult

ExecutionVertex -->  IntermediateResultPartition
```

一个 `ExecutionJobVertex` 可能包含多个（n） 个 `IntermediateResult`， 那实际上每一个并行的子任务 `ExecutionVertex` 可能会会包含（n） 个 `IntermediateResultPartition`。

`IntermediateResultPartition` 的生产者是 `ExecutionVertex`，消费者是一个或若干个 `ExecutionEdge`。

### ExecutionEdge

`ExecutionEdge` 表示 `ExecutionVertex` 的输入，通过 `ExecutionEdge` 将 `ExecutionVertex` 和 `IntermediateResultPartition` 连接起来，进而在不同的 `ExecutionVertex` 之间建立联系。

```java
	private final IntermediateResultPartition source;

	private final ExecutionVertex target;

	private final int inputNum;
```

## 构建 ExecutionGraph 的流程

创建 `ExecutionGraph` 的入口在 `ExecutionGraphBuilder#buildGraph()` 中。

### 1. 创建 ExecutionGraph 对象并设置基本属性

设置 JobInformation, SlotProvider 等信息，下面罗列了一些比较重要的属性：

```java
	/** Job specific information like the job id, job name, job configuration, etc. */
	private final JobInformation jobInformation;

	/** The slot provider to use for allocating slots for tasks as they are needed. */
	private final SlotProvider slotProvider;

	/** The classloader for the user code. Needed for calls into user code classes. */
	private final ClassLoader userClassLoader;

	/** All job vertices that are part of this graph. */
	private final ConcurrentHashMap<JobVertexID, ExecutionJobVertex> tasks;

	/** All vertices, in the order in which they were created. **/
	private final List<ExecutionJobVertex> verticesInCreationOrder;

	/** All intermediate results that are part of this graph. */
	private final ConcurrentHashMap<IntermediateDataSetID, IntermediateResult> intermediateResults;

	/** Current status of the job execution. */
	private volatile JobStatus state = JobStatus.CREATED;

  	/** Listeners that receive messages when the entire job switches it status
	 * (such as from RUNNING to FINISHED). */
	private final List<JobStatusListener> jobStatusListeners;

	/** Listeners that receive messages whenever a single task execution changes its status. */
	private final List<ExecutionStatusListener> executionListeners;
```

### 2. JobVertex 初始化

JobVertex 在 Master 上进行初始化，主要关注`OutputFormatVertex` 和 `InputFormatVertex`，其他类型的 vertex 在这里没有什么特殊操作。File output format 在这一步准备好输出目录, Input splits 在这一步创建对应的 splits。

```java
for (JobVertex vertex : jobGraph.getVertices()) {
			....
			try {
				vertex.initializeOnMaster(classLoader);
			}
			catch (Throwable t) {
					throw new JobExecutionException(jobId,
							"Cannot initialize task '" + vertex.getName() + "': " + t.getMessage(), t);
			}
		}
```

### 4. 生成 ExecutionGraph 内部的节点和连接

对所有的 Jobvertext 进行拓扑排序，并生成 `ExecutionGraph` 内部的节点和连接

```java
//topologically sort the job vertices and attach the graph to the existing one
List<JobVertex> sortedTopology = jobGraph.getVerticesSortedTopologicallyFromSources();
		if (log.isDebugEnabled()) {
			log.debug("Adding {} vertices from job graph {} ({}).", sortedTopology.size(), jobName, jobId);
		}
		executionGraph.attachJobGraph(sortedTopology);
```

#### 4.1 对 JobVertex 进行拓扑排序

所谓拓扑排序，即保证如果存在 A -> B 的有向边，那么在排序后的列表中 A 节点一定在 B 节点之前。具体的算法这里不再详细分析。

#### 4.2 创建 ExecutionJobVertex

按照拓扑排序的结果依次为每个 `JobVertex` 创建对应的 `ExecutionJobVertex`。

```java
		for (JobVertex jobVertex : topologiallySorted) {

			if (jobVertex.isInputVertex() && !jobVertex.isStoppable()) {
				this.isStoppable = false;
			}

			// create the execution job vertex and attach it to the graph
			//创建 ExecutionJobVertex
			ExecutionJobVertex ejv = new ExecutionJobVertex(
				this,
				jobVertex,
				1,
				rpcTimeout,
				globalModVersion,
				createTimestamp);

			//连接上游节点
			ejv.connectToPredecessors(this.intermediateResults);

			ExecutionJobVertex previousTask = this.tasks.putIfAbsent(jobVertex.getID(), ejv);
			if (previousTask != null) {
				throw new JobException(String.format("Encountered two job vertices with ID %s : previous=[%s] / new=[%s]",
					jobVertex.getID(), ejv, previousTask));
			}

			for (IntermediateResult res : ejv.getProducedDataSets()) {
				IntermediateResult previousDataSet = this.intermediateResults.putIfAbsent(res.getId(), res);
				if (previousDataSet != null) {
					throw new JobException(String.format("Encountered two intermediate data set with ID %s : previous=[%s] / new=[%s]",
						res.getId(), res, previousDataSet));
				}
			}

			this.verticesInCreationOrder.add(ejv);
			this.numVerticesTotal += ejv.getParallelism();
			newExecJobVertices.add(ejv);
		}
```

在创建 `ExecutionJobVertex` 的时候会创建对应的 `ExecutionVertex`， `IntermediateResult`，`ExecutionEdge` ， `IntermediateResultPartition` 等对象，这里涉及到的对象相对较多，概括起来大致是这样的：

- 每一个 `JobVertex` 对应一个 ExecutionJobVertex,
- 每一个 `ExecutionJobVertex` 有 parallelism 个 `ExecutionVertex`
- 每一个 `JobVertex` 可能有 n(n>=0) 个 `IntermediateDataSet`，在 `ExecutionJobVertex` 中，一个 `IntermediateDataSet` 对应一个 `IntermediateResult`, 每一个 `IntermediateResult` 都有 parallelism 个生产者, 对应 parallelism 个`IntermediateResultPartition`
- 每一个 `ExecutionJobVertex` 都会和前向的 `IntermediateResult` 连接，实际上是 `ExecutionVertex` 和 `IntermediateResult` 建立连接，生成 `ExecutionEdge`

## 从 ExecutionGraph 到实际运行的任务

`ExecutionGraph` 是在创建 `JobMaster` 时就构建完成的，之后就可以被调度执行了。下面简单概括下调度执行的流程，具体分析见后续的文章。

### ExecutionGraph.scheduleForExecution

按照拓扑顺序为所有的 `ExecutionJobVertex` 分配资源，其中每一个 `ExecutionVertex` 都需要分配一个 slot，`ExecutionVertex` 的一次执行对应一个 `Execution`，在分配资源的时候会依照 `SlotSharingGroup` 和 `CoLocationConstraint` 确定，分配的时候会考虑 slot 重用的情况。

在所有的节点资源都获取成功后，会逐一调用 `Execution.deploy()` 来部署 `Execution`, 使用 `TaskDeploymentDescriptor` 来描述 `Execution`，并提交到分配给该 Execution 的 slot 对应的 TaskManager, 最终被分配给对应的 `TaskExecutor` 执行。



总的来说， `streamGraph` 是最原始的，更贴近用户逻辑的 DAG 执行图；`JobGraph` 是对 `StreamGraph` 的进一步优化，将能够合并的算子合并为一个节点以降低运行时数据传输的开销；`ExecutionGraph` 则是作业运行是用来调度的执行图，可以看作是并行化版本的 `JobGraph`,将 DAG 拆分到基本的调度单元。













