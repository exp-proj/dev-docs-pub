### Maven 添加阿里镜像

`config/settings.xml` 的 `<mirrors>` 标签中添加：

```xml
    <mirror>
      <id>nexus-aliyun</id>
      <mirrorOf>central</mirrorOf>
      <name>Nexus aliyun</name>
      <url>http://maven.aliyun.com/nexus/content/groups/public</url>
    </mirror>
```



