# Kafka 2.5.0基于Docker部署案例

zookeeper采用官方版本：3.5.7

```shell
docker run -d --cpus 0.5 -m 200M \
--restart always \
-u root \
--name zookeeper \
-p 2181:2181 \
-p 2888:2888 \
-p 3888:3888 \
-v /etc/localtime:/etc/localtime \
-v /mnt/zookeeper-3.5.7/data:/data \
-v /mnt/zookeeper-3.5.7/log:/datalog \
-v /mnt/zookeeper-3.5.7/conf:/conf \
zookeeper:3.5.7
```

kafka则使用github使用率较高的wurstmeister/kafka,相对简单，更新到位

```shell
docker run --cpus 4 -d -m 4g \
--restart always \
--name kafka-2.5.0 \
-p 9092:9092 \
--link zookeeper \
-e KAFKA_HEAP_OPTS="-Xmx2g -Xms2g" \
-e KAFKA_ZOOKEEPER_CONNECT=172.19.**.**:2181 \
-e KAFKA_ADVERTISED_HOST_NAME=47.100.***.*** \
-e KAFKA_ADVERTISED_PORT=9092 \
-e KAFKA_LOG_RETENTION_BYTES=10737418240 \
-e KAFKA_LOG_RETENTION_HOURS=24 \
-e KAFKA_DELETE_TOPIC_ENABLE=true \
-e KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR=1 \
-e KAFKA_LOG_DIRS="/kafka/kafka-logs-2.5.0" \
-v /etc/localtime:/etc/localtime \
-v /mnt/kafka-2.5.0:/kafka \
-v /var/run/docker.sock:/var/run/docker.sock \
wurstmeister/kafka:2.12-2.5.0
```

以上，适用于正式环境，单机版吞吐量可以达到千万级，集群版本，只需要按照官方说明配置多启动几个即可，可以参考docker hub 中的 配置说明进行部署。