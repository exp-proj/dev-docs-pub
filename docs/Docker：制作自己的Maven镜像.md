## Docker：制作自己的Maven镜像

>  基于 Windows 10

1. 编写Dockerfile

   宿主机执行命令，创建一个maven-docker文件夹(自定义文件目录与文件名)用来存放配置文件(即settings.xml)以及Dockerfile文件

```dockerfile
FROM maven:3.6.0-jdk-8-alpine
COPY settings.xml /usr/share/maven/ref/
```

2. 编辑 settings.xml, 主要是对maven的repository与mirror进行配置(可以根据自己需求修改)，内容如下

```xml
<?xml version="1.0" encoding="UTF-8"?>

<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
  <localRepository>/usr/share/maven/ref/repository</localRepository>
  <mirrors>
     <mirror>
        <id>aliyun-nexus</id>
        <mirrorOf>central</mirrorOf> 
        <name>Nexus aliyun</name>
        <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
      </mirror>
      <mirror>
        <id>CN</id>
        <name>OSChina Central</name>
        <url>http://maven.oschina.net/content/groups/public/</url>
        <mirrorOf>central</mirrorOf>
      </mirror>
  </mirrors>
</settings>
```

3. 构建镜像

```shell
docker build -t maven-ckai:3.5.4 . # -t 指定镜像名和版本
```

   直到出现 Successfully tagged maven-ckai:3.5.4 说明构建完成，接下来我们来检查一下刚才构建的镜像

```shell
docker images
```

### 使用自定义maven镜像

