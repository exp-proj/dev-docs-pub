## GitLab CI/CD

> 基于windows 的 GitLab Runner,  Java 项目 maven 打包，生成docker 镜像，push 到 docker hub

- 项目中开启 CI/CD 功能页面:  Settings -> General -> Pipelines(打开) ->  Save changes(页面下方)

![image-20201010160809240](./picture/image-20201010160809240.png)

 修改并保存设置后页面：

![image-20201010160935846](./picture/image-20201010160935846.png)



- [Install GitLab Runner](https://docs.gitlab.com/runner/install/) 这里主要记一下 windows 中的安装过程：

  1. 自定义一个目录，可以按照教程中的定义，也可以随便；
  2. 下载 gitlab-runner.exe，如下图 64位的选择 amd64. 下载完成后包名为`gitlab-runner-windows-amd64.exe`; 官方建议重命名为 `gitlab-runner.exe`

  ![image-20201010161417604](./picture/image-20201010161417604.png)

     3. **以管理员身份运行** PowerShell , 进入程序所在目录；执行命令

        ```shell
         .\gitlab-runner.exe install
         .\gitlab-runner.exe start
        ```

        ![image-20201010162343612](./picture/image-20201010162343612.png)

        ![image-20201010162457701](./picture/image-20201010162457701.png)

- [Registering runners](https://docs.gitlab.com/runner/register/) 
  ![image-20201010162646521](./picture/image-20201010162646521.png)

  执行 `./gitlab-runner.exe register`后, 其中前三个是自己需要填写的，值在下图中获取，第四项选择执行器，我用的是`shell`

  ![image-20201010163134953](./picture/image-20201010163134953.png)

  ![image-20201010162927712](./picture/image-20201010162927712.png)
  完成后情况：
  
  ![image-20201010163600424](./picture/image-20201010163600424.png)

- 执行; 只需要在项目根目录中创建 .gitlab-ci.yml文件, push 即可

```yaml
stages:
  - test
  - build

variables:
  DOCKER_DRIVER: overlay2
  VERSION: ${CI_PIPELINE_ID}
  APP_NAME: ${CI_PROJECT_NAMESPACE}-${CI_PROJECT_NAME}
  SUB_PROJECT_NAME: ""
  DOCKER_REGISTRY_PREFIX: ckai

test:
  image: maven:3.3.9-jdk-8
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  stage: test
  script:
    - echo ${CI_PROJECT_NAMESPACE}
    - echo ${CI_PROJECT_PATH}
    - echo ${CI_PROJECT_NAME}
    - echo ${CI_PIPELINE_ID}
    - mvn --version
    - mvn test
  tags:
    - vova-win

build:
  image: maven:3.3.9-jdk-8
  only:
    - master
    - pre_master
    - pre
    - feature/gitlab-ci
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    DOCKER_IMAGE_NAME: ${DOCKER_REGISTRY_PREFIX}/${CI_PROJECT_NAME}:${CI_JOB_ID}-${CI_COMMIT_SHORT_SHA}-${CI_COMMIT_REF_SLUG}
  stage: build
  dependencies:
    - test
  script:
    - mvn clean package
    - mkdir -p docker-build
    - mv ./target/ci-test-1.0.jar ./docker-build
    - cp Dockerfile docker-build/
    - cd docker-build
    - ls
    - echo "Image name:" ${DOCKER_IMAGE_NAME}
    - echo ${USER}
    - pwd
    - docker build -t ${DOCKER_IMAGE_NAME} .
    - docker push ${DOCKER_IMAGE_NAME}
  tags:
    - vova-win
```

![image-20201010164207857](./picture/image-20201010164207857.png)

> 遇到的问题：

1. 因为用的是 windows runner， 所以 shell 命令需要符合windows 的语法
2. 使用到的组件，软件等需要安装完成，如maven ，docker 环境
3. docker push 时可能需要 登陆，第一次登陆后，之后就可以不用再次登陆了

##### GitLab Runner 受使用资源组件限制，建议尝试 docker GitLab Runner 实现



### 问题解决

- #### gitlab-runner 持续集成，docker build 时权限问题 connect: permission denied

原因：gitlab-runner执行时 是以 gitlab-runner用户来执行的 该用户不属于docker group 需将该用户加入该组

解决：

1. 查询 是否有该用户 

```
cut -d : -f 1 /etc/passwd
```

2. 查询 是否存在docker组

```
sudo groupadd docker
```

3. 执行 将gitlab-runner加入docker组

```
sudo gpasswd -a gitlab-runner docker
```

- #### 使用`docker push`镜像时，出现`denied: requested access to the resource is denied`。

原因和`Git push`代码一样，为了安全起见，在`Docker Hub`无法确定操作者的情况下，是无法完成`push`操作的。在`Git`中是通过配置文件`SSH Keys`来记住用户，那么在`Docker Hub`中也是通过配置文件。
通常在你第一次使用`docker login`命令登录你的`Docker`仓库时，会自动在你的机器上生成一个`config.json`的文件，目录具体位置不定。如果你是`root`用户操作，一般在`/root/.docker/config.json`目录。如果是普通用户，那么可能在`~/.docker/config.json`目录上。