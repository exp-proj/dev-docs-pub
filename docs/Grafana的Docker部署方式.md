## Grafana的Docker部署方式

```shell
docker run 
-d 
-p 3001:3000 
--name=grafana544 
-v D:/grafana/grafana-5.0.1/data:/var/lib/grafana 
-v D:/grafana/grafana-5.0.1/conf:/usr/share/grafana/conf 
grafana/grafana:5.4.4
```

以下为参数的说明：

-d : 后台运行容器

-p: 容器的3000端口映射宿主机器的3001端口

--name=自定义容器名称: 设置容器名称

-v 宿主机器文件路径:容器文件路径：挂载共享宿主的文件，实现数据持久化

grafana/grafana:5.4.4: 镜像名称(我使用的是5.4.4版本的镜像)

 

需要注意的是：

1、grafana镜像目前只支持linux版本，所以文件的存放路径与windows版本不一致，挂载时需要注意

2、选择挂载数据文件(/var/lib/grafana)和配置文件(/usr/share/grafana/conf)。前者存放panels、datasoure、页面等数据，后者为配置文件，挂载后可以直接在宿主机器上修改，重启容器后生效



