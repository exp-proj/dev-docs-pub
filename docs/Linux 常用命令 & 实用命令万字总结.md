## Linux常用命令 & 实用命令万字总结

​    原文链接：https://juejin.im/post/5ee444ea6fb9a047ca10f796

> ❝❝
>
> Linux其实是前后端都必会的技能，前端目前很多童鞋都再走大前端路线，后端就更不用说了，小编给大家盘了上万字（由于字数限制本文已删减)关于Linux的实战基础命令，可以说这些都是必须掌握的，当然这些也是不够的，后面小编还会继续更新，大家可以关注我的动态，盘起来，这波可以收藏起来了。
>
> ❞❞

###  ls

最高使用频率的命令之一。

命令格式:  ls [OPTION]... [FILE]... 单纯的输入:

```
[root@iz2ze76ybn73dvwmdij06zz /]# ls
bin   dev  home  lib64       media  opt    proc  run   srv  tmp  var
boot  etc  lib   lost+found  mnt    patch  root  sbin  sys  usr  www
```

##### 加 option -l 它展示了权限，属主，属组，大小，时间，文件名称

```
[root@iz2ze76ybn73dvwmdij06zz /]# ls -l
总用量 68
lrwxrwxrwx    1 root root     7 8月  26 22:36 bin -> usr/bin
dr-xr-xr-x.   5 root root  4096 8月  26 22:39 boot
drwxr-xr-x   19 root root  2960 4月  11 12:59 dev
drwxr-xr-x.  81 root root  4096 9月   5 17:09 etc
drwxr-xr-x.   2 root root  4096 4月  11 12:59 home
lrwxrwxrwx    1 root root     7 8月  26 22:36 lib -> usr/lib
lrwxrwxrwx    1 root root     9 8月  26 22:36 lib64 -> usr/lib64
drwx------.   2 root root 16384 10月 15 2017 lost+found
drwxr-xr-x.   2 root root  4096 4月  11 12:59 media
drwxr-xr-x.   2 root root  4096 4月  11 12:59 mnt
drwxr-xr-x.   3 root root  4096 8月  26 22:40 opt
drwxr-xr-x    2 root root  4096 8月  26 23:00 patch
dr-xr-xr-x  110 root root     0 4月  11 12:59 proc
dr-xr-x---.  12 root root  4096 9月  11 20:59 root
drwxr-xr-x   26 root root   760 9月   7 21:50 run
lrwxrwxrwx    1 root root     8 8月  26 22:36 sbin -> usr/sbin
drwxr-xr-x.   2 root root  4096 4月  11 12:59 srv
dr-xr-xr-x   13 root root     0 4月  11 12:59 sys
drwxrwxrwt.   9 root root  4096 9月  15 03:19 tmp
drwxr-xr-x.  13 root root  4096 8月  26 22:36 usr
drwxr-xr-x.  19 root root  4096 8月  26 22:55 var
drwxr-xr-x    6 root root  4096 8月  26 22:39 www
```

##### ls -a (all)  linux  '.'（点）开头的文件默认是隐藏的，-a可以查询所有的文件。

```
[root@iz2ze76ybn73dvwmdij06zz /]# ls -a
.             bin   etc   lib64       mnt    .pearrc  run   sys    usr
..            boot  home  lost+found  opt    proc     sbin  .test  var
.autorelabel  dev   lib   media       patch  root     srv   tmp    www
```

##### ls -F 以"/"结尾的方式展示文件夹。

```
[root@iz2ze76ybn73dvwmdij06zz /]# ls -F
bin@   dev/  home/  lib64@       media/  opt/    proc/  run/   srv/  tmp/  var/
boot/  etc/  lib@   lost+found/  mnt/    patch/  root/  sbin@  sys/  usr/  www/
```

##### ls -r 倒叙展示，默认的顺序是(a,b,c,d)正序这样展示

```
[root@iz2ze76ybn73dvwmdij06zz /]# ls -r
www  usr  sys  sbin  root  patch  mnt    lost+found  lib   etc  boot
var  tmp  srv  run   proc  opt    media  lib64       home  dev  bin
```

##### ls -R ()递归展示子目录

```
[root@iz2ze76ybn73dvwmdij06zz test]# ls -R
.:
data

./data:
img

./data/img:
car  product  user

./data/img/car:

./data/img/product:

./data/img/user:
```

##### ls -lS 按照文件大小降序显示

```
[root@iz2ze76ybn73dvwmdij06zz /]# ls -lS
总用量 72
drwx------.   2 root root 16384 10月 15 2017 lost+found
dr-xr-xr-x.   5 root root  4096 8月  26 22:39 boot
drwxr-xr-x.  81 root root  4096 9月   5 17:09 etc
drwxr-xr-x.   2 root root  4096 4月  11 12:59 home
drwxr-xr-x.   2 root root  4096 4月  11 12:59 media
drwxr-xr-x.   2 root root  4096 4月  11 12:59 mnt
drwxr-xr-x.   3 root root  4096 8月  26 22:40 opt
drwxr-xr-x    2 root root  4096 8月  26 23:00 patch
dr-xr-x---.  12 root root  4096 9月  15 22:03 root
drwxr-xr-x.   2 root root  4096 4月  11 12:59 srv
drwxr-xr-x    3 root root  4096 9月  15 22:10 test
drwxrwxrwt.   9 root root  4096 9月  15 03:19 tmp
drwxr-xr-x.  13 root root  4096 8月  26 22:36 usr
drwxr-xr-x.  19 root root  4096 8月  26 22:55 var
drwxr-xr-x    6 root root  4096 8月  26 22:39 www
drwxr-xr-x   19 root root  2960 4月  11 12:59 dev
drwxr-xr-x   26 root root   760 9月   7 21:50 run
lrwxrwxrwx    1 root root     9 8月  26 22:36 lib64 -> usr/lib64
lrwxrwxrwx    1 root root     8 8月  26 22:36 sbin -> usr/sbin
lrwxrwxrwx    1 root root     7 8月  26 22:36 bin -> usr/bin
lrwxrwxrwx    1 root root     7 8月  26 22:36 lib -> usr/lib
dr-xr-xr-x  110 root root     0 4月  11 12:59 proc
dr-xr-xr-x   13 root root     0 4月  11 12:59 sys
```

##### ls -g 不输出所有者(属主)信息

```
[root@iz2ze76ybn73dvwmdij06zz /]# ls -g
总用量 72
lrwxrwxrwx    1 root     7 8月  26 22:36 bin -> usr/bin
dr-xr-xr-x.   5 root  4096 8月  26 22:39 boot
-rw-r--r--    1 root     0 9月  15 22:20 c
drwxr-xr-x   19 root  2960 4月  11 12:59 dev
drwxr-xr-x.  81 root  4096 9月   5 17:09 etc
drwxr-xr-x.   2 root  4096 4月  11 12:59 home
lrwxrwxrwx    1 root     7 8月  26 22:36 lib -> usr/lib
lrwxrwxrwx    1 root     9 8月  26 22:36 lib64 -> usr/lib64
drwx------.   2 root 16384 10月 15 2017 lost+found
drwxr-xr-x.   2 root  4096 4月  11 12:59 media
drwxr-xr-x.   2 root  4096 4月  11 12:59 mnt
drwxr-xr-x.   3 root  4096 8月  26 22:40 opt
drwxr-xr-x    2 root  4096 8月  26 23:00 patch
dr-xr-xr-x  110 root     0 4月  11 12:59 proc
dr-xr-x---.  12 root  4096 9月  15 22:18 root
drwxr-xr-x   26 root   760 9月   7 21:50 run
lrwxrwxrwx    1 root     8 8月  26 22:36 sbin -> usr/sbin
drwxr-xr-x.   2 root  4096 4月  11 12:59 srv
dr-xr-xr-x   13 root     0 4月  11 12:59 sys
drwxr-xr-x    3 root  4096 9月  15 22:18 test
drwxrwxrwt.   9 root  4096 9月  15 03:19 tmp
drwxr-xr-x.  13 root  4096 8月  26 22:36 usr
drwxr-xr-x.  19 root  4096 8月  26 22:55 var
drwxr-xr-x    6 root  4096 8月  26 22:39 www
```

##### ls -lG 隐藏所有组（属组）信息

```
[root@iz2ze76ybn73dvwmdij06zz /]# ls -lG
总用量 72
lrwxrwxrwx    1 root     7 8月  26 22:36 bin -> usr/bin
dr-xr-xr-x.   5 root  4096 8月  26 22:39 boot
-rw-r--r--    1 root     0 9月  15 22:20 c
drwxr-xr-x   19 root  2960 4月  11 12:59 dev
drwxr-xr-x.  81 root  4096 9月   5 17:09 etc
drwxr-xr-x.   2 root  4096 4月  11 12:59 home
lrwxrwxrwx    1 root     7 8月  26 22:36 lib -> usr/lib
lrwxrwxrwx    1 root     9 8月  26 22:36 lib64 -> usr/lib64
drwx------.   2 root 16384 10月 15 2017 lost+found
drwxr-xr-x.   2 root  4096 4月  11 12:59 media
drwxr-xr-x.   2 root  4096 4月  11 12:59 mnt
drwxr-xr-x.   3 root  4096 8月  26 22:40 opt
drwxr-xr-x    2 root  4096 8月  26 23:00 patch
dr-xr-xr-x  110 root     0 4月  11 12:59 proc
dr-xr-x---.  12 root  4096 9月  15 22:18 root
drwxr-xr-x   26 root   760 9月   7 21:50 run
lrwxrwxrwx    1 root     8 8月  26 22:36 sbin -> usr/sbin
drwxr-xr-x.   2 root  4096 4月  11 12:59 srv
dr-xr-xr-x   13 root     0 4月  11 12:59 sys
drwxr-xr-x    3 root  4096 9月  15 22:18 test
drwxrwxrwt.   9 root  4096 9月  15 03:19 tmp
drwxr-xr-x.  13 root  4096 8月  26 22:36 usr
drwxr-xr-x.  19 root  4096 8月  26 22:55 var
drwxr-xr-x    6 root  4096 8月  26 22:39 www
10.ls -li 显示文件的索引号
[root@iz2ze76ybn73dvwmdij06zz /]# ls -li
```

##### ls -li 显示文件的索引号

```
[root@iz2ze76ybn73dvwmdij06zz /]# ls -li

总用量 72
  7628 lrwxrwxrwx    1 root root     7 8月  26 22:36 bin -> usr/bin
1179650 dr-xr-xr-x.   5 root root  4096 8月  26 22:39 boot
  4923 -rw-r--r--    1 root root     0 9月  15 22:20 c
  1026 drwxr-xr-x   19 root root  2960 4月  11 12:59 dev
917505 drwxr-xr-x.  81 root root  4096 9月   5 17:09 etc
655362 drwxr-xr-x.   2 root root  4096 4月  11 12:59 home
    17 lrwxrwxrwx    1 root root     7 8月  26 22:36 lib -> usr/lib
    13 lrwxrwxrwx    1 root root     9 8月  26 22:36 lib64 -> usr/lib64
    11 drwx------.   2 root root 16384 10月 15 2017 lost+found
786434 drwxr-xr-x.   2 root root  4096 4月  11 12:59 media
131074 drwxr-xr-x.   2 root root  4096 4月  11 12:59 mnt
393219 drwxr-xr-x.   3 root root  4096 8月  26 22:40 opt
1966081 drwxr-xr-x    2 root root  4096 8月  26 23:00 patch
     1 dr-xr-xr-x  110 root root     0 4月  11 12:59 proc
131073 dr-xr-x---.  12 root root  4096 9月  15 22:18 root
   218 drwxr-xr-x   26 root root   760 9月   7 21:50 run
    15 lrwxrwxrwx    1 root root     8 8月  26 22:36 sbin -> usr/sbin
393220 drwxr-xr-x.   2 root root  4096 4月  11 12:59 srv
     1 dr-xr-xr-x   13 root root     0 4月  11 12:59 sys
2097153 drwxr-xr-x    3 root root  4096 9月  15 22:18 test
393217 drwxrwxrwt.   9 root root  4096 9月  15 03:19 tmp
1048578 drwxr-xr-x.  13 root root  4096 8月  26 22:36 usr
262145 drwxr-xr-x.  19 root root  4096 8月  26 22:55 var
1441793 drwxr-xr-x    6 root root  4096 8月  26 22:39 www
```

##### ls -lt 按照修改时间排序（倒序）依次创建文件 a b

```
[root@iz2ze76ybn73dvwmdij06zz test]# ls -lt
总用量 0
-rw-r--r-- 1 root root 0 9月  15 22:18 b
-rw-r--r-- 1 root root 0 9月  15 22:18 a
```

##### ls -version

```
[root@iz2ze76ybn73dvwmdij06zz test]# ls --version
ls (GNU coreutils) 8.22
Copyright (C) 2013 Free Software Foundation, Inc.
许可证：GPLv3+：GNU 通用公共许可证第3 版或更新版本<http://gnu.org/licenses/gpl.html>。
本软件是自由软件：您可以自由修改和重新发布它。
在法律范围内没有其他保证。

由Richard M. Stallman 和David MacKenzie 编写。
```

### cat

cat,又是一个相对高频率的命令。比较官方的说法是命令用于链接文件并打印到标准输出设备上。这个文本输出命令可以用来查看文件内容，创建内容等，下面我们介绍几个比较实用的用法。命令格式 : cat [选项]... [文件]...

##### cat -n 查看并显示行号

```
[root@iz2ze76ybn73dvwmdij06zz linuxdemo]# cat -n a
    1  this is a txt
    2  hello every body
    3  hello world
    4  hahaha
```

##### cat 创建一个空文件 b

```
[root@iz2ze76ybn73dvwmdij06zz linuxdemo]# cat >b <<EOF
> EOF
[root@iz2ze76ybn73dvwmdij06zz linuxdemo]# ls
a  b
```

##### cat 清空文件内容

```
[root@iz2ze76ybn73dvwmdij06zz linuxdemo]# cat >a<<EOF
> EOF
[root@iz2ze76ybn73dvwmdij06zz linuxdemo]# cat a
```

##### cat 写入内容(如果原来有内容将被覆盖)

```
[root@iz2ze76ybn73dvwmdij06zz linuxdemo]# cat >a<<EOF
> today is a good day
> because off work
> hahaha
> EOF
[root@iz2ze76ybn73dvwmdij06zz linuxdemo]# cat a
today is a good day
because off work
hahaha
```

##### cat 同时显示多个文件内容( b中写出happy)

```
[root@iz2ze76ybn73dvwmdij06zz linuxdemo]# cat a > b
[root@iz2ze76ybn73dvwmdij06zz linuxdemo]# cat b
today is a good day
because off work
hahaha
```

##### 追加内容 可以看到用了连个>以后内容是追加的

```
[root@iz2ze76ybn73dvwmdij06zz linuxdemo]# cat a >> b
[root@iz2ze76ybn73dvwmdij06zz linuxdemo]# cat b
today is a good day
because off work
hahaha
today is a good day
because off work
hahaha
```

##### cat --help (如想学习更多，可以自己动手尝试)

```
[root@iz2ze76ybn73dvwmdij06zz linuxdemo]# cat --help
用法：cat [选项]... [文件]...
将[文件]或标准输入组合输出到标准输出。

 -A, --show-all           等于-vET
 -b, --number-nonblank    对非空输出行编号
 -e                       等于-vE
 -E, --show-ends          在每行结束处显示"$"
 -n, --number             对输出的所有行编号
 -s, --squeeze-blank      不输出多行空行
 -t                       与-vT 等价
 -T, --show-tabs          将跳格字符显示为^I
 -u                       (被忽略)
 -v, --show-nonprinting   使用^ 和M- 引用，除了LFD和 TAB 之外
     --help    显示此帮助信息并退出
     --version    显示版本信息并退出

如果没有指定文件，或者文件为"-"，则从标准输入读取。

示例：
 cat f - g  先输出f 的内容，然后输出标准输入的内容，最后输出g 的内容。
 cat        将标准输入的内容复制到标准输出。

GNU coreutils online help: <http://www.gnu.org/software/coreutils/>
请向<http://translationproject.org/team/zh_CN.html> 报告cat 的翻译错误
要获取完整文档，请运行：info coreutils 'cat invocation'
```

###  用户权限

大家都知道linux是一个多任务，多用户的操作系统。多个用户可以在同一时间操作系统执行不同的任务，一个用户也可以多个人登陆操作。一般来说root的权限最大,我们经常使用的数据库一般会用一个mysql的用户,网页相关的我们可能用一个www的账号,大家都是知道删除跑路的故事，因此有一些操作权限是要交给一些专人管理的。为了系统的学习，我觉得有必要了解一下它的基本知识。

##### linux角色分类

超级用户:root (user id --UID) 0 最高的管理权限。

普通用户：

```
    系统用户 UID:1-999(rhel7  centos7) 1-499(rhel6)
    
    本地用户 UID:1000+(rhel7 centos7)  500+(rhel6)
```

UID:用户身份标示,唯一标示。

##### linux组分类

组：

组分类:

```
根据账号的功能分类:

           超级用户组：root GID:0
           普通用户组 ：
                               系统用户组: GID 1---999
                                本地用户组: GID 1000+
```

##### linux用户和组的关系

一个用户属于一个组，也可以属于多个组，多个用户可以在一个组，可以在多个组。

##### linux用户和组的相关配置文件

1./etc/passwd:用户 and 属性信息。

2./etc/group: 组 and 属性信息。

3./etc/shadow: 用户密码 and 属性信息。

4./etc/gshadow:组密码 and 属性信息。

5./etc/default/useradd: 设置添加用户规则文件。

6./etc/login.defs:设置用户账号限制。

##### linux用户信息文件

用户管理中最重要的一个文件，这个文件是对所有用户开放的，每一行存一个用户的信息，每个属性之间用冒号分割。

```
cat /etc/passwd
root:x:0:0:root:/root:/bin/zsh
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
```

举例: root:x:0:0:root:/root:/bin/zsh

1.登陆用户名:root

2.用户密码占位符:x

3.用户UID:0

4.用户组GID:0

5.对用户账户的描述:root

6.用户家目录位置:/root

7.用户默认的shell:/bin/zsh (默认是/bin/bash)

##### linux密码信息文件

该文件只有root用户有读权限，每一行存一个用户的记录，每个属性用冒号分割。

```
cat /etc/shadow
root:$6$LxE7qiaZ$ZZXf/aCO1Zqs1oOS9WDutUk9rFOq4MOoSG9IeXq2f5sduGrgfYvzOWHZmJZFGC.0Of1DjCna0M.oQZTY8r00/.:17769:0:99999:7:::
bin:*:17110:0:99999:7:::
daemon:*:17110:0:99999:7:::
```

举例: bin:*:17110:0:99999:7::: 1.登录用户名: bin

2.加密的密码:  * 号代表密码被锁定

3.最近更改密码的日期: 17110代表到1970-1-1密码不可改的天数

4.密码修改期限：99999 代表永远不用改,如果是其他数字则表示从1970-1-1内的多少天必须修改密码。

5.更改密码最大有效天数:  代表密码保持有效的最大天数。

6.密码过期警告:密码到正式失效前有多少天(-1,永远不提示)

7.密码过期后多少天禁用用户:可登陆，单不能操作。

8.用户被禁用日期:多少天后账号过期，不能登陆。

9.保留参数

##### linux组信息文件

用户组的组信息存放在这，一行存一个组记录，属性用冒号分割。

```
➜  ~ cat /etc/group
root:x:0:
bin:x:1:
daemon:x:2:
sys:x:3:
adm:x:4:
tty:x:5:
disk:x:6:
lp:x:7:
mem:x:8:
kmem:x:9:
```

举例: root:x:0:

1.组名：root

2.组密码占位符: x

3.组GID: 0

###  安装包管理基础

相信大部分人应该和我一样，最开始都是用 windows上手的，那在windows平台的安装包就再熟悉不过了，比如.exe,是不是太常见了，一般在windows安装过mysql的，相信.msi也是见过的。大多数的服务器都是linux的,因此我们来学习一下linux的安装包基础知识。不同发行版的系统用的工具也是不相同的。目前大多数的公司服务器是基于centos,因此后续会以centos为例比较多。

##### 基于redhat

yum

zypper

rpm

##### 基于centos

yum

rpm

##### rpm

全称: Redhat Package Manager

下载网址: http://rpmfind.net

![img](data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQImWNgYGBgAAAABQABh6FO1AAAAABJRU5ErkJggg==)

先来看下包的命名格式,以  rpm-5.4.10-10.i586.rpm 为例。

rpm : 软件包名

5 : 主版本号

4 : 次版本号

10 : 修订号

i586 : cpu架构平台

rpm : 包后缀

##### yum

解决软件包安装的依赖关系问题，自动下载软件包，基于c/s架构。repo文件是yum源的配置文件，改文件定义软件仓库的信息。为什么说它是c/s架构，原来rpm软件的头(header)里会记录改软件的依赖信息，因此可以对头内容进行分析,就可以知道其相关的依赖，并将这些信息记录下来。那么服务端是提供这些rpm的软件包，然后分析包的依赖关系，将记录存在服务器某目录上。客户端在安装软件时下载其记录依赖关系文件并分析，最后一次性下载。

###  进程管理基础

一般来说程序分为两类，一种是系统程序，一种是应用程序。一个运行了程序，就可以说是一个进程了，进程是占用内存空间的，而当你杀掉进城时，资源也会随之释放。

##### 进程的类型

用户进程：用户自己的程序,用户可以控制他的开启和关闭。

交互进程:  与用户进行交互操作的进程。

批处理进程：是一个进程集合，按顺序启动其他进程。

守护进程：一直运行的进程.crond。

##### 进程的属性

进程ID : (pid) 唯一的数字标示，区分不同的进程。

进程有父进程和子进程。

启动进程的用户ID(uid)和用户的属组。

进程的三种状态:

- 运行 -- R
- 休眠 -- S
- 僵尸 -- Z

进程的优先级: 取值范围(-20,19) ,数值越小优先级越大,默认为0。

进程链接的终端。

进程占用资源情况。

##### 父子进程的关系

父子进程的关系,父亲管理儿子。父进程终止时子进程一定终止,而子进程终止时父进程不一定终止。

##### 进程管理工具

```
    ps:   查看进程。
    top:  可以查看进程的动态信息。
    kill:  杀进程。
    pstree:  查看进程树。
    pgrep:  搜进程。
    lsof:  查看进程打开的文件。
```

###  进程管理实践

##### ps

静态的显示当前进程的信息 。

命令:  ps

语法:  ps 参数

常用参数:

```
               a      显示所有用户的进程
               r       显示运行中的进程
               l        长格式输出
               u        按用户名和启动时间的顺序来显示进程
               f         用树形格式来显示进程
               x        显示没有控制终端的进程
```

操作: BSD格式来显示进程

```
➜  ~ ps aux
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root         1  0.0  0.0  43364  3596 ?        Ss   11月03   0:01 /usr/lib/syst
root         2  0.0  0.0      0     0 ?        S    11月03   0:00 [kthreadd]
root         3  0.0  0.0      0     0 ?        S    11月03   0:00 [ksoftirqd/0]
root         5  0.0  0.0      0     0 ?        S<   11月03   0:00 [kworker/0:0H
root         7  0.0  0.0      0     0 ?        S    11月03   0:00 [migration/0]
root         8  0.0  0.0      0     0 ?        S    11月03   0:00 [rcu_bh]
root         9  0.0  0.0      0     0 ?        S    11月03   0:05 [rcu_sched]
root        10  0.0  0.0      0     0 ?        S    11月03   0:00 [watchdog/0]
root        11  0.0  0.0      0     0 ?        S    11月03   0:00 [watchdog/1]
root        12  0.0  0.0      0     0 ?        S    11月03   0:00 [migration/1]
root        13  0.0  0.0      0     0 ?        S    11月03   0:00 [ksoftirqd/1]
root        15  0.0  0.0      0     0 ?        S<   11月03   0:00 [kworker/1:0H
```

标准格式显示进程unix风格

```
 ~ ps -ef
UID        PID  PPID  C STIME TTY          TIME CMD
root         1     0  0 11月03 ?      00:00:01 /usr/lib/systemd/systemd --syste
root         2     0  0 11月03 ?      00:00:00 [kthreadd]
root         3     2  0 11月03 ?      00:00:00 [ksoftirqd/0]
root         5     2  0 11月03 ?      00:00:00 [kworker/0:0H]
root         7     2  0 11月03 ?      00:00:00 [migration/0]
root         8     2  0 11月03 ?      00:00:00 [rcu_bh]
root         9     2  0 11月03 ?      00:00:05 [rcu_sched]
root        10     2  0 11月03 ?      00:00:00 [watchdog/0]
root        11     2  0 11月03 ?      00:00:00 [watchdog/1]
root        12     2  0 11月03 ?      00:00:00 [migration/1]
root        13     2  0 11月03 ?      00:00:00 [ksoftirqd/1]
root        15     2  0 11月03 ?      00:00:00 [kworker/1:0H]
root        17     2  0 11月03 ?      00:00:00 [kdevtmpfs]
root        18     2  0 11月03 ?      00:00:00 [netns]
root        19     2  0 11月03 ?      00:00:00 [khungtaskd]
root        20     2  0 11月03 ?      00:00:00 [writeback]
root        21     2  0 11月03 ?      00:00:00 [kintegrityd]
```

##### 属性详解:

USER:   进程的属主

PID:   进程的ID

%CPU:  进程占cpu百分比

%MEM:  进程占内存的百分比

VSZ:   进程占用虚拟内存大小

RSS:   固定内存使用数量

STAT   进程状态

```
       R    正在运行可中在队列中可过行的； S    处于休眠状态；      T    停止或被追踪；      Z    僵尸进程；      N    优先级较低的进程      L    有些页被锁进内存；      s    进程的领导者（在它之下有子进程）
```

``



START  启动进程的时间；

TIME  进程消耗CPU的时间；

COMMAND 命令的名称和参数;

按照指定属性排序:

按照cpu从小到大排序。

```
➜  ~ ps aux --sort %cpu
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root         1  0.0  0.0  43364  3596 ?        Ss   11月03   0:01 /usr/lib/syst
root         2  0.0  0.0      0     0 ?        S    11月03   0:00 [kthreadd]
root         3  0.0  0.0      0     0 ?        S    11月03   0:00 [ksoftirqd/0]
root         5  0.0  0.0      0     0 ?        S<   11月03   0:00 [kworker/0:0H
root         7  0.0  0.0      0     0 ?        S    11月03   0:00 [migration/0]
root         8  0.0  0.0      0     0 ?        S    11月03   0:00 [rcu_bh]
root         9  0.0  0.0      0     0 ?        S    11月03   0:05 [rcu_sched]
root        10  0.0  0.0      0     0 ?        S    11月03   0:00 [watchdog/0]
root        11  0.0  0.0      0     0 ?        S    11月03   0:00 [watchdog/1]
```

按照cpu从大到小排序。

```
➜  ~ ps aux --sort -%cpu
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root     21342  0.3  0.4 137184 17112 ?        Ssl  03:24   1:16 /usr/local/aegi
root         1  0.0  0.0  43364  3596 ?        Ss   11月03   0:01 /usr/lib/syst
root         2  0.0  0.0      0     0 ?        S    11月03   0:00 [kthreadd]
root         3  0.0  0.0      0     0 ?        S    11月03   0:00 [ksoftirqd/0]
root         5  0.0  0.0      0     0 ?        S<   11月03   0:00 [kworker/0:0H
root         7  0.0  0.0      0     0 ?        S    11月03   0:00 [migration/0]
root         8  0.0  0.0      0     0 ?        S    11月03   0:00 [rcu_bh]
root         9  0.0  0.0      0     0 ?        S    11月03   0:05 [rcu_sched]
root        10  0.0  0.0      0     0 ?        S    11月03   0:00 [watchdog/0]
root        11  0.0  0.0      0     0 ?        S    11月03   0:00 [watchdog/1]
root        12  0.0  0.0      0     0 ?        S    11月03   0:00 [migration/1]
root        13  0.0  0.0      0     0 ?        S    11月03   0:00 [ksoftirqd/1]
```

##### top

与ps相反的是top命令可以查看进程的动态信息。

命令:  top

语法:  top 参数

常用参数:

操作:

```
➜  ~ top
top - 11:38:42 up 13:15,  2 users,  load average: 0.01, 0.03, 0.05
Tasks:  98 total,   1 running,  97 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.2 us,  0.0 sy,  0.0 ni, 99.8 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem :  3881688 total,   229264 free,   543932 used,  3108492 buff/cache
KiB Swap:  1049596 total,  1049596 free,        0 used.  3026944 avail Mem

 PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
   1 root      20   0   43364   3596   2340 S   0.0  0.1   0:01.79 systemd
   2 root      20   0       0      0      0 S   0.0  0.0   0:00.00 kthreadd
   3 root      20   0       0      0      0 S   0.0  0.0   0:00.14 ksoftirqd/0
   5 root       0 -20       0      0      0 S   0.0  0.0   0:00.00 kworker/0:+
   7 root      rt   0       0      0      0 S   0.0  0.0   0:00.33 migration/0
   8 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcu_bh
   9 root      20   0       0      0      0 S   0.0  0.0   0:05.73 rcu_sched
```

##### 属性解释:

前五行是数据的整理统计信息。

第一行:

```
     11:38:42     当前时间
     
      up 13:15     系统运行时间,格式为时:分
      
      2 user         当前登陆用户数量
      
      load average: 0.01, 0.03, 0.05  系统负载,任务队列平均长度 1分钟,5分钟,15分钟前到现在的平均值。
```

第二三行：

```
98 total  进程总数量
 1 running 正在运行的进程数量
 97 sleeping 睡眠的进程数量
 0 stopped  停止的进程数量
 0 zombie   僵尸进程数量
 %Cpu(s):
 0.2 us 系统用户进程使用cpu百分比
 0.0 sy 内核进程占用cpu百分比
 0.0 ni 用户进程空间内改变过优先级的进程占用cpu百分比
     99.8 id 空闲cpu百分比
       0.0 wa 等待输入输出的cpu时间百分比
       0.0 hi   硬件cpu中断占用百分比
       0.0  si   软中断占用百分比
       0.0  st  虚拟机占用百分比
```

第四五行:

Mem :  (单位K)

```
3881688 total  物理内存总量
229264 free    空闲内存总量
543932 used    使用的物理内存总量
3108492 buff/cache 内核缓存的内存量
```

Swap: (单位K)

```
 1049596 total      交换区总量
 1049596 free       空闲交换区总量
 0 used             使用的交换区总量
 3026944 avail Mem  可利用的内存量
```

标题行:

PID:  进程id

USER: 进程所有者用户名

PR:  优先级

NI:  进程优先级，nice值，负值 -> 高优先级，正值 -> 低优先级

VIRT: 虚拟内存总量 virt=swap + res

RES:  实际使用内存大小

SHR:  共享内存大小

S:   进程状态

```
   d: 不可中断的睡眠状态
   r: 运行
   s: 睡眠
   t: 跟踪
   z: 僵尸进程
```

%CPU: 上次更新到现在cpu时间占用百分比

%MEM: 进程使用物理内存百分比

TIME+: 进程使用cpu的时间总计，单位 1/100秒

COMMAND: 命令行

##### kill

命令: kill

语法:

```
      kill [-s signal|-p] [-q sigval] [-a] [--] pid...
      kill -l [signal]
```

常用参数:

```
     -l 列出所有信号名称
     -s 指定发送信号（默认）
     -u 指定用户
```

操作: 列出所有信号名称

```
➜  ~ kill -l
HUP INT QUIT ILL TRAP ABRT BUS FPE KILL USR1 SEGV USR2 PIPE ALRM TERM STKFLT CHLD CONT STOP TSTP TTIN TTOU URG XCPU XFSZ VTALRM PROF WINCH POLL PWR SYS
```

强制终止

```
➜  ~ kill -9 23423
终止
➜  ~ kill -15 22323
```

杀死指定用户的所有进程

```
➜  ~ kill -u superh
```

##### proc目录

这个目录中有内核，进程运行状态的信息。

```
➜  /proc ls
1      21320  21628  240   392   7          diskstats    loadavg       swaps
10     21342  21976  242   42    722        dma          locks         sys
10353  21465  21977  243   44    7561       driver       mdstat        sysrq-trigger
10509  21613  21978  244   442   8          execdomains  meminfo       sysvipc
107    21614  22     249   45    803        fb           misc          timer_list
11     21615  220    26    46    8179       filesystems  modules       timer_stats
11010  21616  22285  261   466   8307       fs           mounts        tty
11102  21617  22287  262   467   8327       interrupts   mtrr          uptime
12     21618  22361  267   469   9          iomem        net           version
13     21619  22363  268   476   acpi       ioports      pagetypeinfo  vmallocinfo
14461  21620  22396  3     477   buddyinfo  irq          partitions    vmstat
15     21621  22467  31    485   bus        kallsyms     sched_debug   zoneinfo
17     21622  225    3198  5     cgroups    kcore        schedstat
18     21623  229    32    505   cmdline    keys         scsi
19     21624  23     33    506   consoles   key-users    self
2      21625  23884  336   5217  cpuinfo    kmsg         slabinfo
20     21626  23885  34    5515  crypto     kpagecount   softirqs
21     21627  24     357   65    devices    kpageflags   stat
```

上面的每个标号即是用我们当前进程的pid所命名的目录名。

##### free

查看系统使用和空闲的内存情况。

命令:  free

语法:  free 参数

常用参数:

```
 -b, --bytes        byte为单位展示
 -k, --kilo            k为单位展示
 -m, --mega       m为单位展示
 -g, --giga           g为单位展示
```

操作:

```
➜  ~ free -m
             total        used        free      shared  buff/cache   available
Mem:           3790         536         210           2        3043        2950
Swap:          1024           0        1024
```

单位为g显示。

```
➜  ~ free -h
            total        used        free      shared  buff/cache   available
Mem:           3.7G        538M        207M        2.4M        3.0G        2.9G
Swap:          1.0G          0B        1.0G
```

### 文件查找

在做运维相关的事情时，我们少不了要查这样那样的文件，一般来说我们可能会查比如带xxx.conf名字的文件在哪呢,查日志啊，某个可执行文件的位置啊。

##### 概览

```
which : 查看执行文件的位置。

whereis : 查看可执行文件位置和相关文件。

locate : 配合数据库缓存，快速查看文件的位置。

grep : 过滤匹配，他是一个文件搜索工具。

find  :  可以根据条件查看文件。
```

##### which

命令：which

语法:   which 参数  查询目标

常用命令: 一般不加参数使用

```
          --all, -a      显示所有的匹配路径
```

操作:

```
➜  ~ which pwd
/usr/bin/pwd
```

###  whereis

命令:  whereis

语法:  whereis 参数 查询目标

常用命令:

```
           一般不加参数使用
         -b         只搜索二进制文件
         -B <目录>  定义二进制文件查找路径
         -m         只搜索 man 手册
         -M <目录>  定义 man 手册查找路径
         -s         只搜索源代码
         -S <目录>  定义源代码查找路径
```

操作:

```
➜  ~ whereis ls
ls: /usr/bin/ls /usr/share/man/man1/ls.1.gz
```

##### locate

命令:  locate

语法:  locate  文件名

常用参数: 无

操作: locate和find命令功能差不多，但是搜索效率更高，因为locate查的是数据库而find查找的是目录文件。

数据库:

```
➜  ~ ls /var/lib/mlocate/mlocate.db
/var/lib/mlocate/mlocate.db
```

相关配置文件:

```
➜  ~ ls /etc/updatedb.conf
/etc/updatedb.conf
```

相关定时任务:

```
➜  ~ ls /etc/cron.daily/mlocate
/etc/cron.daily/mlocate
```

举例:

```
➜  ~ touch shafa
➜  ~ locate shafa
➜  ~ updatedb
➜  ~ locate shafa
/root/shafa
```

注：如果我们当天新建的文件查找，需要手动updatedb。

##### grep

命令：grep

语法：grep 参数  目标值  文件

常用参数:

```
            -v       取反
            -i        忽略大小写
            -n        输出的同时打印行号
            ^*        以*开头
            *$         以*结尾
            ^$         空行
```

操作:

查 /etc/passwd有root的行

```
➜  ~ grep root /etc/passwd
root:x:0:0:root:/root:/bin/zsh
operator:x:11:0:operator:/root:/sbin/nologin
```

查 /etc/passwd 没有root的行(内容较多，禁展示几行)

```
➜  ~ grep -v root /etc/passwd
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
```

查 /etc/passwd 有www的行并显示行号

```
➜  ~ grep -n www /etc/passwd
23:www:x:1000:1000::/home/www:/sbin/nologin
```

##### find

命令：find

语法：find 路径  参数 输出

常用参数:

```
            路径: 例如用 . 来表示当前目录，用 / 来表示系统根目录。
           -print：显示的时候”\n”做为定界符, 换行
          -print0：与xargs配套使用，以“\0”做为定界符
```

find常用命令选项:

```
-name   按照文件名查找文件。“名称”
-perm   按照文件权限来查找文件。666 777 等
-depth  在查找文件时，首先查找当前目录中的文件，然后再在其子目录中查找
-user   按照文件属主来查找文件
 -atime  -ctime (单位是天)
 -mmin -cmin  -amin(单位是分钟)
 -size n [c] 查找文件长度为n块的文件，带有c时表示文件长度以字节计
 -follow  如果find命令遇到符号链接文件，就跟踪至链接所指向的文件。
```

操作:

查看当前目录下test.开头的文件

```
➜  ~ find . -name "test*"
./test.txt
./autojump/tests
./test.1
```

查看当前目录下test.开头的文件,输出结果换行显示(默认)

```
➜  ~ find . -name "test*" -print
./test.txt
./autojump/tests
./test.1
```

查看当前目录下test.开头的文件,输出结果不换行显示

```
➜  ~ find . -name "test*" -print0
./test.txt./autojump/tests./test.1
```

对查找的内容执行相应命令

默认执行动作 : -print

常用动作:

```
              -exec  这个参数后可以跟自定义shell命令
```

操作:

查询.txt结尾的文件并用ls查询其更多属性

```
➜  test find . -name "*.txt" -exec ls -l {} \;
-rw-r--r-- 1 root root 0 11月  4 14:56 ./xq1.txt
-rw-r--r-- 1 root root 0 11月  4 14:56 ./xq.txt
-rw-r--r-- 1 root root 0 11月  4 14:56 ./xq2.txt
```

查询.txt结尾的文件并变成.bak结尾的文件

```
➜  test find . -name "*.txt" -exec mv {} {}.bak \;
➜  test ls
xq1.txt.bak  xq2.txt.bak  xq.txt.bak
```

逻辑查询:

```
        -a  并且
        -o  或者
         +  高于
         -   低于
```

操作：查看 .sh或者.q结尾的文件。

```
➜  test find . -name "*.sh" -o -name "*.q"
./book.q
./stop.sh
./start.sh
```

查看.sh结尾并且s开头的文件。

```
➜  test find . -name "*.sh" -a -name "s*"
./stop.sh
./start.sh
```

在文件中插入内容后查看。

```
➜  test ll
总用量 12K
-rw-r--r-- 1 root root  9 11月  4 15:10 book.q
-rw-r--r-- 1 root root 13 11月  4 15:10 start.sh
-rw-r--r-- 1 root root  4 11月  4 15:10 stop.sh
-rw-r--r-- 1 root root  0 11月  4 14:56 xq1.txt.bak
-rw-r--r-- 1 root root  0 11月  4 14:56 xq2.txt.bak
-rw-r--r-- 1 root root  0 11月  4 14:56 xq.txt.bak
```

查看/etc 大于40k小于50k的文件

```
➜  test find /etc/ -size +40k -a -size  -50k
/etc/selinux/targeted/active/modules/100/sysadm/hll
/etc/selinux/targeted/contexts/files/file_contexts.homedirs.bin
```

###  计划任务

说到计划,联想到的是时间,说到任务,联想到的就是做什么。连起来就是什么时间做什么。每天几点上班几点下班。每年的国庆，元旦都在固定的时间节点发生。类似这些,我们可以称之为定时任务。

再比如,有的时候。快下班了,领导来个紧急任务,小王啊,加个班。明天心情好，请个假去面试。这种的属于临时任务。

那么在linux中我们怎么来做这两件事呢。

##### 任务类型

定时:  按照一定的周期重复持续的做某个事。

临时:  执行一次以后任务就结束了，只一次。

##### 任务类型对应的命令

crontab( 定时任务): 按照你 你规定的周期 循环窒息感。

at (临时任务): 执行一次就结束的任务。

##### at的使用

命令：at

语法:   at 时间

常用参数:

```
         -l    查看任务列表
         -c   查看任务详情
```

操作：查看服务是否开启

```
[root@iz2ze0ajic0vbv28hcdctpz ~]# systemctl status atd
● atd.service - Job spooling tools
  Loaded: loaded (/usr/lib/systemd/system/atd.service; enabled; vendor preset: enabled)
  Active: active (running) since 二 2018-11-13 11:50:47 CST; 1 day 9h ago
Main PID: 491 (atd)
  CGroup: /system.slice/atd.service
          └─491 /usr/sbin/atd -f

11月 13 11:50:47 iz2ze0ajic0vbv28hcdctpz systemd[1]: Started Job spooling tools.
11月 13 11:50:47 iz2ze0ajic0vbv28hcdctpz systemd[1]: Starting Job spooling tools...
```

几种设定任务的方法: 创建一个任务(默认是今天,如果时间大于当前时间会创建在下一天 ）

```
[root@iz2ze0ajic0vbv28hcdctpz ~]# ls
soft
[root@iz2ze0ajic0vbv28hcdctpz ~]# at 21:17
at> mkdir sk
at> <EOT>
job 6 at Wed Nov 14 21:17:00 2018
```

指定时间创建:

```
[root@iz2ze0ajic0vbv28hcdctpz ~]# at 20:00 2018-11-19
at> mkdir happy
at> <EOT>
job 7 at Mon Nov 19 20:00:00 2018
```

从现在开始加时间:

```
[root@iz2ze0ajic0vbv28hcdctpz ~]# at now + 3min
at> mkdir hello
at> <EOT>
job 8 at Wed Nov 14 21:41:00 2018
```

明天11:11 执行

```
[root@iz2ze0ajic0vbv28hcdctpz ~]# at 11:11 tomorrow
at> mkdir world
at> <EOT>
job 9 at Thu Nov 15 11:11:00 2018
```

2天后的下午3.24执行

```
[root@iz2ze0ajic0vbv28hcdctpz ~]# at 3:24 pm +2 days
at> mkdir new
at> <EOT>
job 11 at Fri Nov 16 15:24:00 2018
```

查看任务列表(显示还未执行的任务 ）

```
[root@iz2ze0ajic0vbv28hcdctpz ~]# atq
5  Thu Nov 15 21:11:00 2018 a root
```

查看一个单个的任务信息:(由于内容较多去掉部分信息)

```
[root@iz2ze0ajic0vbv28hcdctpz /]# at -c 7
#!/bin/sh
# atrun uid=0 gid=0
# mail root 0
umask 22
XDG_SESSION_ID=243; export XDG_SESSION_ID
HOSTNAME=iz2ze0ajic0vbv28hcdctpz; export HOSTNAME
SHELL=/bin/bash; export SHELL
HISTSIZE=1000; export HISTSIZE
SSH_CLIENT=113.46.167.195\ 17211\ 22; export SSH_CLIENT
SSH_TTY=/dev/pts/0; export SSH_TTY
USER=root; export USER
MAVEN_HOME=/usr/local/apache-maven-3.6.0; export MAVEN_HOME
MAIL=/var/spool/mail/root; export MAIL
PATH=/usr/local/apache-maven-3.6.0/bin:/usr/local/jdk1.8.0_191/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin; export PATH
PWD=/root; export PWD
JAVA_HOME=/usr/local/jdk1.8.0_191; export JAVA_HOME
LANG=zh_CN.UTF-8; export LANG
HISTCONTROL=ignoredups; export HISTCONTROL
SHLVL=1; export SHLVL
HOME=/root; export HOME
LOGNAME=root; export LOGNAME
CLASSPATH=.:/lib/dt.jar:/lib/tools.jar; export CLASSPATH
SSH_CONNECTION=113.46.167.195\ 17211\ 172.17.187.244\ 22; export SSH_CONNECTION
LC_CTYPE=zh_CN.UTF-8; export LC_CTYPE
LESSOPEN=\|\|/usr/bin/lesspipe.sh\ %s; export LESSOPEN
XDG_RUNTIME_DIR=/run/user/0; export XDG_RUNTIME_DIR
cd /root || {
  echo 'Execution directory inaccessible' >&2
  exit 1
}
${SHELL:-/bin/sh} << 'marcinDELIMITER550b4fbb'
mkdir happy

marcinDELIMITER550b4fbb
```

参数解释:

5 : 任务编号

Thu Nov 15 21:11:00 2018 : 时间

a : 队列

root : 执行用户

删除任务：

先来查看一下有哪些任务

```
[root@iz2ze0ajic0vbv28hcdctpz /]# atq
7  Mon Nov 19 20:00:00 2018 a root
5  Thu Nov 15 21:11:00 2018 a root
11  Fri Nov 16 15:24:00 2018 a root
9  Thu Nov 15 11:11:00 2018 a root
12  Wed Nov 14 22:11:00 2018 a root
```

删除任务编号为12的任务

```
[root@iz2ze0ajic0vbv28hcdctpz /]# atrm 12
[root@iz2ze0ajic0vbv28hcdctpz /]# atq
7  Mon Nov 19 20:00:00 2018 a root
5  Thu Nov 15 21:11:00 2018 a root
11  Fri Nov 16 15:24:00 2018 a root
9  Thu Nov 15 11:11:00 2018 a root
```

### 定时任务处理

cron是linux的一个定时任务执行工具,而crondtab是周期性执行命令,crond则是定时任务的守护进程。

##### 服务启动

```
 查看服务是否启动
[root@iz2ze0ajic0vbv28hcdctpz ~]# systemctl status crond
● crond.service - Command Scheduler
  Loaded: loaded (/usr/lib/systemd/system/crond.service; enabled; vendor preset: enabled)
  Active: active (running) since 二 2018-11-13 11:50:47 CST; 1 day 10h ago
Main PID: 489 (crond)
  CGroup: /system.slice/crond.service
          └─489 /usr/sbin/crond -n

11月 13 11:50:47 iz2ze0ajic0vbv28hcdctpz systemd[1]: Started Command Scheduler.
11月 13 11:50:47 iz2ze0ajic0vbv28hcdctpz systemd[1]: Starting Command Scheduler...
11月 13 11:50:47 iz2ze0ajic0vbv28hcdctpz crond[489]: (CRON) INFO (RANDOM_DELAY will be scaled with factor 7...ed.)
11月 13 11:50:47 iz2ze0ajic0vbv28hcdctpz crond[489]: (CRON) INFO (running with inotify support)
Hint: Some lines were ellipsized, use -l to show in full.
如果没有启动,执行
systemctl start crond
systemctl enable crond
```

##### crontab命令

语法 :  crontab  参数

常用参数  :

```
        crontab -u   #指定用户的cron信息
        crontab -l   #列出当前用户下的cron服务的信息
        crontab -u user -l   #列出指定用户的cron服务的信息
        crontab -r   #删除cron服务
        crontab -e   #编辑cron服务
        crontab -r -u user   #删除指定用户的定时任务
```

操作:

编辑cron服务

```
[root@iz2ze0ajic0vbv28hcdctpz ~]# crontab -e
crontab: installing new crontab
```

查看当前用户下任务内容

```
[root@iz2ze0ajic0vbv28hcdctpz ~]# crontab -l
22 22 22 * * mkdir /linux/test
```

删除任务

```
[root@iz2ze0ajic0vbv28hcdctpz ~]# crontab -r
[root@iz2ze0ajic0vbv28hcdctpz ~]# crontab -l
no crontab for root
```

查看qa这个用户下的任务内容

```
[root@iz2ze0ajic0vbv28hcdctpz ~]# crontab -u qa -l
22 22 22 * * mkdir /linux/test
```

删除qa这个用户下的定时任务

```
[root@iz2ze0ajic0vbv28hcdctpz ~]# crontab -u qa -r
[root@iz2ze0ajic0vbv28hcdctpz ~]# crontab -u qa -l
no crontab for qa
```

##### 系统任务

系统的任务调度配置文件在/etc/crontab下

```
[root@iz2ze0ajic0vbv28hcdctpz ~]# ll /etc/crontab
-rw-r--r-- 1 root root 451 Nov 15 07:49 /etc/crontab
```

可以直接在/etc/crontab下添加系统任务(不建议)

```
[root@iz2ze0ajic0vbv28hcdctpz ~]# cat /etc/crontab
SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=root

# For details see man 4 crontabs

# Example of job definition:
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  *  * user-name  command to be executed
```

定时任务相关的目录

```
[root@iz2ze0ajic0vbv28hcdctpz etc]# ll cr
cron.d/       cron.deny     cron.monthly/ cron.weekly/
cron.daily/   cron.hourly/  crontab       crypttab
```

目录作用解释:

```
cron.d/       是系统自动定期需要做的任务(不是按照时，分，秒等时间周期执行的)
cron.deny   在该文件内的用户无法做计划任务
其他文件为不同周期的执行脚本。
```

##### 定时任务规则

首先来看下任务的格式(如图):

![img](data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQImWNgYGBgAAAABQABh6FO1AAAAABJRU5ErkJggg==)

时间的一些特殊符号:![img](data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQImWNgYGBgAAAABQABh6FO1AAAAABJRU5ErkJggg==)

下面我们来举一些例子:

```
#每晚的20:30重加载nginx
30 20 * * * /usr/local/nginx/sbin/nginx -s reload

#每月1、10、15日的5:30重加载nginx
30 5 1,10,15 * * /usr/local/nginx/sbin/nginx  -s reload

#每天20: 00至22 : 00之间每隔30分钟重加载nginx
0,30 20-22 * * * /usr/local/nginx/sbin/nginx -s reload

每星期六的10 : 00 pm 重加载nginx
0 22* * 6 /usr/local/nginx/sbin/nginx  -s reload

#每一小时重加载一次nginx
0 */1 * * * /usr/local/nginx/sbin/nginx -s reload
```

### expect

很多时候我们和linux的操作交互需要人工干预，有时候我们可能需要一种工具可以不需要人工干预的和linux交互，这里引入了一个工具expect,本小结我们主要来讲如何使用expect。

##### expect介绍

TCL(tool command language)是一种类似shell脚本的语言,你可以使用他来完成许多操作，expect是在它的基础上发展而来的，expect还提供了一些tcl没有的命令。如果你要写一个自动处理输入输出的脚本，我建议选择expect。

##### expect安装

yum install -y expect

##### expect使用

```
1.定义脚本执行使用的shell。

     #!/usr/bin/expect


2.设置超时时间，单位为秒，timeout -1 为永不超时。

        set timeout 2

3.壳子

       spawn需要在expect环境下运行，用来传递交互指令。

 4.expect命令

       判断输出结果是否包含某字符串，没有立即返回，否则就等一段时间后返回，用timeout设置等待时间。

5.send

        执行交互动作，将交互要执行的动作进行输入给交互指令。命令字符串结尾要加上"\r"，如果出现异常等待的状态可以进行核查。

6.exp_continue

         继续执行接下来的交互操作。

7.interact

        执行后保持交互状态，把控制权交给控制台，如不加interact，交互完成会自动退出。
```

##### 免密码通过ssh登陆服务器

操作脚本

```
➜  batch cat sf
#!/usr/bin/expect -f
spawn ssh  root@ip
expect {
"yes/no" { send "yes\r";exp_continue}
"*password:" { send "passwordstr\r" }
}
interact
```

执行查看结果，登陆成功

```
➜  batch ./35
spawn ssh root@ip
root@ip's password:
Last login: Sat Jan  5 10:15:56 2019 from 113.46.187.213

Welcome to Alibaba Cloud Elastic Compute Service !
```



> ❝❝
>
> 这些只是Linux的冰上一角，小编还在路上，持续更新。。。⛽️
>
> ❞

