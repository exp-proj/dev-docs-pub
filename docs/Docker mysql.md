#### Docker mysql

==============本地路径映射到docker容器路径下====
##### 下载 mysql
docker pull mysql

##### 运行容器：
docker run -p 3306:3306 --name mysql -v D:\DockerWorkspace\mysql\conf:/etc/mysql/conf.d -v D:\DockerWorkspace\mysql\logs:/logs -v D:\DockerWorkspace\mysql\data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=root -d mysql

##### 进入容器：
docker exec -it mysql bash

##### 登录mysql
mysql -u root -p
ALTER USER 'root'@'localhost' IDENTIFIED BY 'root';

##### 添加远程登录用户
CREATE USER 'ckai'@'%' IDENTIFIED WITH mysql_native_password BY 'ckai';

GRANT ALL PRIVILEGES ON *.* TO 'ckai'@'%';

##### 客户端连接测试
ckai/ckai 的远程账号登录