## [深入探究 K8S ConfigMap 和 Secret](https://mp.weixin.qq.com/s?__biz=MzI3NTEwOTA4OQ==&mid=2649177319&idx=1&sn=cbee5bf6baf94d39b3e47449038c95e0&chksm=f31a5969c46dd07fcde841b66d75d0097948a2ab178a3c6fe3a58dcc9f9dc1844bb9d46bd0b1&scene=21#wechat_redirect)

**1、什么是 ConfigMap？**

ConfigMap 是用来存储配置文件的 Kubernetes 资源对象，配置对象存储在 Etcd 中，配置的形式可以是完整的配置文件、key/value 等形式。



**2、ConfigMap 能带来什么好处？**

传统的应用服务，每个服务都有自己的配置文件，各自配置文件存储在服务所在节点，对于单体应用，这种存储没有任何问题，但是随着用户数量的激增，一个节点不能满足线上用户使用，故服务可能从一个节点扩展到十个节点，这就导致，如果有一个配置出现变更，就需要对应修改十次配置文件。这种人肉处理，显然不能满足线上部署要求，故引入了各种类似于 ZooKeeper 中间件实现的配置中心，但配置中心属于 “侵入式” 设计，需要修改引入第三方类库，它要求每个业务都调用特定的配置接口，破坏了系统本身的完整性，而Kubernetes 利用了 Volume 功能，完整设计了一套配置中心，其核心对象就是ConfigMap，使用过程不用修改任何原有设计，即可无缝对 ConfigMap；为什么呢？

![img](./picture/640)

如图（1）所示，

- ConfigMap 相当于放入原生应用的配置文件，可以是一个或者多个；

- 容器启动之后，到宿主机中拉取 ConfigMap 的内容，生成本地文件，通过 volume 形式映射到容器内部指定目录上；

- 容器中应用程序按照原有方式读取容器特定目录上的配置文件。

在容器看来，配置文件就像是打包在容器内部特定目录，整个过程对应用没有任何侵入。



**3、ConfigMap 三种创建方式**

- 指定字面量进行创建，创建命令如下所示：

```
kubectl create configmap configmaptest --from-literal=foo=bar --from-literal=one=two
```

​    创建完成后通过如下方式查看：

```yaml
[root@k8s-master k8s]# kubectl get configmap configmaptest -o yaml
apiVersion: v1
data:
  foo: bar
  one: two
kind: ConfigMap
metadata:
  creationTimestamp: "2020-04-14T13:53:42Z"
  name: configmaptest
  namespace: default
  resourceVersion: "613402"
  selfLink: /api/v1/namespaces/default/configmaps/configmaptest
  uid: 59b91eb4-7e57-11ea-83c7-509a4c36e19d
```

- 指定特定文件进行创建

```shell
kubectl create configmap config-files --from-file=/home/conf/db.properties
```

   可以通过如下方式进行查看，（内容过长，影响阅读，省略 ConfigMap 元信息。）

```yaml
[root@k8s-master k8s]# kubectl get configmap test-config -o yaml
apiVersion: v1
data:
  db.properties: |
    driverClassName=com.mysql.jdbc.Driver
    ......
```

- 指定特定文件夹进行创建

```
kubectl create configmap config-dir --from-file=/home/conf/config-test
```

​    通过如下方式进行查看

```yaml
[root@k8s-master k8s]# kubectl get configmap config-test -o yaml
apiVersion: v1
data:
  db.properties: |
    # 数据源配置
    driverClassName=com.mysql.jdbc.Driver
   ......
  logback.xml: "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<configuration debug=\"true\"
   ......
  svc.properties: |
    #server
    protocol=tcp
    .......
  system.properties: |
    time=100
    .......
```

如上描述三种基本的 ConfigMap创建方式，当然也可以使用合并不同选项进行创建配置文件，具体如下所示：

- 

```
kubectl create configmap config-mix --from-file=/home/conf/biz/ --from-file=/home/conf/db.xml  --from-literal=one=two
```



看到这么多，你可能会想到，--from-file最后一级如果是文件夹会怎样呢？如你所想，文件夹其实不会被包含，只会查找最后一级目录下的文件。

**4、ConfigMap 作为环境变量三种使用方式**

- 单个引用

1、首先创建 ConfigMap

```
kubectl create configmap configmaptest --from-literal=code=25 --from-literal=foo=bar --from-literal=one=two
```

2、Deployment yaml中引用 ConfigMap 设置环境变量，如图（2）所示

![img](./picture/640wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

3、通过如下方式进行查看，环境变量是否生效，可以发现，容器环境中已经存在引用ConfigMap中的环境变量

```
[root@k8s-master k8s]# kubectl exec nginx-7c958f6448-z5q56 -it /bin/bash[root@nginx-7c958f6448-z5q56 /]# env|grep CODECODE-TIME=25
```

- 多个引用

1、一次性传递所有ConfigMap条目作为环境变量，如图（3）所示

![img](./picture/hvZjCFh6diaSLA80WPM4cq7QwdAicnn5zrCNlzzjwtw14D5apsRxLujZEsyNHGvkwEXiciaBQSJo8sGEcyrgTibm34Q)

可以通过如下方式进行查看环境变量是否生效，如下所示每个环境变量都按照预设，添加了配置的前缀，有人可能要说，我的配置文件中原来是什么配置现在还保留什么配置，不需要添加预设前缀，那么请查看如图（4）通过把前缀设置为空串，即可保持原有配置方式。

```
[root@k8s-master k8s]# kubectl exec nginx-84ccdff98d-vgzcw -it /bin/bash[root@nginx-84ccdff98d-vgzcw /]# env|grep CODECODE_foo=barCODE_code=25CODE_one=two
```

![img](./picture/hvZjCFh6diaSLA80WPM4cq7QwdAicnn5zrIhZwwlrEDUraZlUcFuPicrp6UpZThghFgv2S0Ujk88x7jyibFu9MaCMg)



- args 方式传递环境变量

容器启动时，传递该变量到服务，运行 shell 脚本，可能会用到，具体设置方式如图（5）所示：

![img](./picture/hvZjCFh6diaSLA80WPM4cq7QwdAicnn5zrQQ6fUphJsdmO7WAHG6DsoyVRgk4lxPa5shdSr2u0skGrjAYVUl727w)



以上解释了通过在 yaml 设置 env 引用 ConfigMap 中配置作为环境变量的使用，在使用过程中，我参考了 《Kubernetes In Action》这本书，发现此书中有一段是这样描述的，如图（6）所示：

![img](./picture/hvZjCFh6diaSLA80WPM4cq7QwdAicnn5zrzQVqibO9OiaI6apic2M1ibTeOn6tJ3HX6yJ93MRgejvib1shffVoMrwhZ1w)

其大概意思是，配置键中不能包含破折号，如果包含则不能设置到环境变量中，此书这部分是基于 Kubernetes 1.6 进行编撰，而我使用的是 kubernetes 1.14，不清楚是不是因为 Kubernetes 已经改进的原因，还是其他原因，我有两点不解的地方。破折号（——）大多都是指特别长的符号，在编码过程中很少有人使用这个，即使使用了，Kubernetes 根本无法保存成功。又何谈环境变量一说呢？会提示如图(7)，图（8）所示错误：

![img](./picture/hvZjCFh6diaSLA80WPM4cq7QwdAicnn5zrQTlfpRakUziaJlgBic31GoibYZ3azIicFce33fvH8QNEqyzV1XUma2rzBw)

![img](./picture/hvZjCFh6diaSLA80WPM4cq7QwdAicnn5zr5pSicXNjqpIw4pUK32mPcgX8M3UG05Q2h0gsu2ILloK9FXEKKvwgU5A)

如果破折号换成英文半角字符 - 中划线呢？如图（9）所示，是可以保存成功的。当然也可以用于环境变量中。

![img](data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQImWNgYGBgAAAABQABh6FO1AAAAABJRU5ErkJggg==)

当然通过如上方式设置完成之后，就可以直接在容器内部使用环境变量读取已经设置的配置，但是使用环境变量的方式有一个致命的缺点是，当外部 ConfigMap 更新配置完成之后，容器内部环境变量并不会随之改变，这是因为 ENV 是容器启动时候注入的，启动之后 Kubernetes 就不会改变 ENV 的值，即配置不能同步更新，只能通过重启容器方式，配置才能生效。



**5、挂载 volume**

这种方式则是通过 volume 形式映射到容器内部指定目录上，容器内部进程直接读取该目录下特定文件，这种方式是我们常用的一种方式，具体使用时如下所示：

```yaml
  ......
        - containerPort: 80
        volumeMounts:
          - mountPath: /usr/local/nginx/conf/vhost/
            name: http
          - mountPath: /usr/local/nginx/html/foo
            subPath: foo
            name: nginx-html
      volumes:
      - name: http
        configMap:
          name: nginx-conf
      - name: nginx-html
        configMap:
          name: configmaptest
          items: 
          - key: foo
            path: foo
   ......
```

volumeMounts 是容器内部指定挂载目录，volumes 是引用目录，即宿主机设置 ConfigMap 文件地址。



- 可以直接挂载一个目录到容器内部，当宿主机通过如下方式修改 configmap 那么容器内部配置将随之改变，一次性修改所有文件。但是使用这种方式有一个问题需要注意，如果挂载到容器内部的文件夹下存在其它文件，这种挂载方式将直接覆盖原有文件夹下的文件。

```
[root@k8s-master ~]# kubectl edit configmap configmaptestconfigmap/configmaptest edited
```

 

- 如果有特定需求，需要挂载某个特定文件，而不允许覆盖原有文件，可以挂载到指定文件，通过 subPath 配合指定文件。但是单个文件挂载这种方式不能实现热更新，即宿主机 ConfigMap 文件发生变化，容器内部不会自动重载。

- 至于 items 使用就比较简单了，如果一个 ConfigMap 中包含多个配置文件，但是只想暴露出来其中一部分，那么可以通过 items 方式进行指定。当然你也可以对文件设置读写权限。

**5、Secret 使用**

Secret 使用类似于 ConfigMap，支持两种形式的使用：

- 将 Secret 作为环境变量暴露给容器进程使用。
- 将 Secret 通过volume 数据卷提供给容器进程使用。

看到这里你可能要说了，什么都一样，为啥还要 Secret，一个 ConfigMap 解决问题不就完事了，其实不然，Secret 顾名思义，是用于存储加密数据的，老版本 Kubernetes 只支持 base64 加密，学过计算机的都知道 base64 那就不是什么加密，只是对字符串进行了 encode 编码，通过 decode 直接可以解出明文。但后来新版本的 Kubernetes 已经实现了真正意义上的加解密，所以 Secret 存在是有一定意义的，使用方式跟 ConfigMap 类似，但是命令确不一样。

1、创建 Secret 输入如下：

```
kubectl create secret generic nginx-ssl --from-file=ca.key--from-file=ca.cert
```

2、查看 Secret 输入如下所示：

```shell
[root@k8s-master ~]# kubectl get secret
NAME                  TYPE                                  DATA   AGE
default-token-7h5z9   kubernetes.io/service-account-token   3      6d13h
nginx-ssl         Opaque                                2      21h
[root@k8s-master ~]# kubectl get secret nginx-ssl -o yaml
apiVersion: v1
data:
  ca.crt: QmFnIEF0dHJpYnV0ZXMKICAgIGZy.......................
  ca.key: QmFnIEF0dHJpYnV0ZXMKICAgIGZy......................
kind: Secret
.....................
```

3、Pod 引用方式：

```yaml
.....................
- containerPort: 80
        volumeMounts:
          - mountPath: /home/nginx/nginx/conf/cert/
            name: nginx-ssl
      volumes:
      - name: nginx-ssl
        secret:
          secretName: nginx-ssl
.....................
```

**6、应用程序怎么做到不重启情况下读取最新配置**

上面已经提及使用环境变量和单文件挂载形式，无法实现热更新，但是通过 数据卷形式可以实现宿主机和 Pod 内部读取配置的实时更新，但是有一点需要注意的是 ConfigMap 更新，数据卷也更新了，如果你的应用进程不进行配置重载，即实时读取配置数据，同样还是使用的老配置。这个问题可以通过把 Pod 的副本数减少到 0 进行重建 Pod 解决。这种方式虽然能够解决服务重新加载问题，但是也会带来问题。因为可能会导致同一套服务，配置不一致的问题，因此，如果业务对实时性要求高，建议改成服务实时加载配置。

总结一下，Kubernetes 只是把配置实时同步到数据卷配置文件中，至于加载时机，还要看自己的应用程序。



举个例子，nginx 配置存储在 Kubernetes ConfigMap 里面，公钥信息存储在 Secret 中，nginx 充当服务里面的反向代理，因为端口资源规划问题，需要修改 nginx 配置文件中端口，修改完成后，Pod 中的数据卷配置信息发生变化，但 nginx 并不会重载已经修改的配置信息。通过如下命令行修改，修改完成后，发现 Pod 中 nginx 配置生效。

```
kubectl exec nginx -c nginx -- /usr/local/nginx/sbin/nginx -s reload
```

**7、总结**

本文主要说明了 Kubernetes 中 ConfigMap 和 Secret 使用以及使用过程中需要注意的问题， ConfigMap 本身是一个很接地气的设计，它借助于 volume ，原有服务不用修改任何代码，即可无缝对接。如果你已经使用了其它分布式配置管理服务，如：DisConf，Apollo等，你也可以保持原有方式，继续使用。