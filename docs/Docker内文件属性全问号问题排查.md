起因：用docker cp 命令将本地文件移动到docker内部后，发现文件属性全部是问号。无法更改属主、权限等，也无法删除。

多方查找，在宿主机上，将docker容器下底层的镜像目录包含了一个diff文件，diff目录存放了当前层的镜像内容，找到相应的文件，正常删除即可。
diff 中没有时，也可以在 merged 目录中操作

类似路径为：/var/lib/docker/overlay2/dcb02e3fXXXXXXXXXXXX/diff

该路径可通过docker inspect dockerName | grep UpperDir 获得。