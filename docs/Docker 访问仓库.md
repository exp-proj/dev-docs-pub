### Docker 访问仓库

>  仓库（Repository）是集中存放镜像的地方。

> 目前 Docker 官方维护了一个公共仓库 Docker Hub，其中已经包括了超过 15,000 的镜像。大部分需求，都可以通过在 Docker Hub 中直接下载镜像来实现。

#### 私有仓库

> docker-registry 是官方提供的工具，可以用于构建私有的镜像仓库。

##### 容器互联 

>  使用 `--link` 参数可以让容器之间安全的进行交互。

- 先创建一个新的数据库容器。 

```shell
$ sudo docker run -d --name db training/postgres
```

- 然后创建一个新的 web 容器，并将它连接到 db 容器

```shell
$ sudo docker run -d -P --name web --link db:db training/webapp
python app.py
```

- 此时，db 容器和 web 容器建立互联关系。 

> `--link` 参数的格式为 `--link name:alias` ，
>
> - 其中 name 是要链接的容器的名称， 
> - alias 是这个连接的别名。

- 使用 docker ps 来查看容器的连接

> Docker 在两个互联的容器之间创建了一个安全隧道，而且不用映射它们的端口到 宿主主机上。在启动 db 容器的时候并没有使用 -p 和 -P 标记，从而避免了暴 露数据库端口到外部网络上。 

- Docker 通过 2 种方式为容器公开连接信息： 

  - 环境变量 

  - 更新 /etc/hosts 文件 

- 使用 env 命令来查看 web 容器的环境变量

```shell
 $ sudo docker run --rm --name web2 --link db:db training/webapp env 
 . . . 
 DB_NAME=/web2/db 
 DB_PORT=tcp://172.17.0.5:5432 
 DB_PORT_5000_TCP=tcp://172.17.0.5:5432 
 DB_PORT_5000_TCP_PROTO=tcp 
 DB_PORT_5000_TCP_PORT=5432 
 DB_PORT_5000_TCP_ADDR=172.17.0.5 . . . 
 # 其中 DB_ 开头的环境变量是供 web 容器连接 db 容器使用，前缀采用大写的连接 别名。
```

- 除了环境变量，Docker 还添加 host 信息到父容器的 /etc/hosts 的文件。下面 是父容器 web 的 hosts 文件

```shell
$ sudo docker run -t -i --rm --link db:db training/webapp /bin/bash
root@aed84ee21bde:/opt/webapp# cat /etc/hosts
172.17.0.7 aed84ee21bde
. . .
172.17.0.5 db
# 这里有 2 个 hosts，第一个是 web 容器，web 容器用 id 作为他的主机名，第二个 是 db 容器的 ip 和主机名。 可以在 web 容器中安装 ping 命令来测试跟db容器的连 通。 
root@aed84ee21bde:/opt/webapp# apt-get install -yqq inetutils-ping
root@aed84ee21bde:/opt/webapp# ping db
PING db (172.17.0.5): 48 data bytes
56 bytes from 172.17.0.5: icmp_seq=0 ttl=64 time=0.267 ms
56 bytes from 172.17.0.5: icmp_seq=1 ttl=64 time=0.250 ms
56 bytes from 172.17.0.5: icmp_seq=2 ttl=64 time=0.256 ms
# ping 来测试db容器，它会解析成 172.17.0.5 。 *注意：官方的 ubuntu 镜像默认没有安装 ping，需要自行安装。
```





















