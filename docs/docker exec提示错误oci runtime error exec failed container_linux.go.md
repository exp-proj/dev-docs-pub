# docker exec提示错误oci runtime error: exec failed: container_linux.go

```
sudo docker exec -it  569f05d5f4fc /bin/bash 
```

提示错误

```
rpc error: code = 13 desc = invalid header field value "oci runtime error: 
\ exec failed: container_linux.go:247: 
\ starting container process caused \"exec: \\\"/bin/bash\\\": 
\ stat /bin/bash: no such file or directory\"\n"1234
```

尝试

```
sudo docker exec -it  569f05d5f4fc /bin/sh 
```

or

```
sudo docker exec -it  569f05d5f4fc bash
```