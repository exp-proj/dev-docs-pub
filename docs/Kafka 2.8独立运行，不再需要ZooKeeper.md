## Kafka 2.8独立运行，不再需要ZooKeeper

![图片](./picture/wx_fmt=jpeg&tp=webp)

分布式发布与订阅系统Apache Kafka在即将发布的2.8版本，使用Kafka内部的Quorum控制器来取代ZooKeeper，因此用户第一次可在完全不需要ZooKeeper的情况下执行Kafka，这不只节省运算资源，并且也使得Kafka效能更好，还可支持规模更大的集群。



过去Apache ZooKeeper是Kafka这类分布式系统的关键，ZooKeeper扮演协调代理的角色，所有代理服务器启动时，都会连接到Zookeeper进行注册，当代理状态发生变化时，Zookeeper也会储存这些数据，在过去，ZooKeeper是一个强大的工具，但是毕竟ZooKeeper是一个独立的软件，使得Kafka整个系统变得复杂，因此官方决定使用内部Quorum控制器来取代ZooKeeper。



这项工作从去年4月开始，而现在这项工作取得部分成果，用户将可以在2.8版本，在没有ZooKeeper的情况下执行Kafka，官方称这项功能为Kafka Raft元数据模式（KRaft）。在KRaft模式，过去由Kafka控制器和ZooKeeper所操作的元数据，将合并到这个新的Quorum控制器，并且在Kafka集群内部执行，当然，如果使用者有特殊使用情境，Quorum控制器也可以在专用的硬件上执行。



KRaft协定使用事件驱动机制来追踪整个集群的元数据，过去必须依赖RPC来处理的任务，现在受益于事件驱动以及实际的日志传输，这些改变所带来的好处，便是让Kafka仍够支持更多的分割。



过去Kafka因为带着ZooKeeper，因此被认为拥有笨重的基础设施，而在移除ZooKeeper之后，Kafka更轻巧更适用于小规模工作负载，轻量级单体程序适合用于边缘以及轻量级硬件解决方案。



值得注意的是，在抢先体验版中，有部分像是ACL、安全以及交易等功能都尚未支持，而且在KRaft模式下，也还不支持重新分配分割和JBOD，官方提到，这些功能会在今年稍晚的版本中提供，由于很多功能处于测试阶段，不建议大家将其用于生产环境中。