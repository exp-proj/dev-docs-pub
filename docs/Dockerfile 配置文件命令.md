#### Dockerflie 命令配置

- FROM 指定基础镜像

- RUN 执行命令
  - shell 格式
  - exec 格式
- COPY 复制文件
  
  - COPY 和 ADD 选择的原则：所有的文件复制均使用 COPY，仅在需要自动解压的场合使用 ADD
- ADD 更高级的复制文件
  - 相对 COPY  可以通过链接获取文件，然后放在目标路径中；如果是`tar`压缩文件，会自动解压
  - ADD 会令镜像构建缓存失效，从而可能会令镜像构建变得比较缓慢
  - 最适合场景为：需要自动解压缩的场合

-  WORKDIR 指定工作目录
- CMD 容器启动命令
  
- 用于指定默认的容器主进程的启动命令
  
- ENTRYPOINT 入口点

  - 场景一： 让镜像变成像命令一样使用： 存在 ENTRYPOINT 后，CMD 的内容将会作为参数传给 ENTRYPOINT ，而后面的参数会作为参数传给 ENTRYPOINT 中的 CMD 
  - 场景二： 应用运行前的准备工作： 

- ENV 设置环境变量：

  > ENV <key> <value> 

  > ENV <key1>=<value1> <key2>=<value2>... 

  ```dockerfile
  ENV VERSION=1.0 DEBUG=on \    # 换行
  NAME="Happy Feet"             # 含有空格的值需要用双引号括起来
  ```

  - 使用的时候 用  `$VERSION`
  -  ADD, COPY, ENV, EXPOSE, LABEL, USER, WORKDIR, VOLUME, STOPSIGNAL, ONBUILD 支持环境变量展开

- ARG 构建参数

  >  ARG <参数名>[=<默认值>] 

  - ARG 设置的构建环境的环境变量，在将来容器运行时是不会存在这些环境变量的。但不用因此使用`ARG` 保存密码之类的信息，因为 `docker history` 还是可以看的所有的值的。
  - 该默认值可以在构建命令 `docker build` 中用 `--build-arg <参数名>=<值>` 来覆盖

- VOLUME 定义匿名卷

  -  VOLUME ["<路径1>", "<路径2>"...]  
  - VOLUME <路径> 

  ```dockerfile
  VOLUME /data # 这里的 /data 就会在运行时自动挂载为匿名卷，任何向 /data 中写入的数据都不会记录进容器存储层，从而保证了容器存储层的无状态化。当然，运行时可以覆盖这个挂载设置，比如
  docker run -d -v mydata:/data xxxx # 该命令使用 mydata 这个命名卷挂载到了 /data 这个位置，替代了 Dockerfile 中定义的匿名卷的挂载配置
  ```

- EXPOSE 声明端口

  -  EXPOSE <端口1> [<端口2>...] 
  - EXPOSE 指令声明运行时容器提供服务端口，这只是一个声明，在运行时并不会因为这个声明应用就会开启这个端口的服务
  - 在 Dockerfile 中写入这样的声明有两个好处，
    - 一个是帮助镜像使用者理解这个镜像服务的守护端口，以方便配置映
      射；
    - 另一个用处则是在运行时使用随机端口映射时，也就是 docker run -P
      时，会自动随机映射 EXPOSE 的端口
  - 要将 EXPOSE 和在运行时使用 -p <宿主端口>:<容器端口> 区分开来。 
    - -p ，是 映射宿主端口和容器端口，换句话说，就是将容器的对应端口服务公开给外界访 问，
    - 而 EXPOSE 仅仅是声明容器打算使用什么端口而已，并不会自动在宿主进行 端口映射。

- WORKDIR 指定工作目录

  - WORKDIR <工作目录路径>
  - 使用 WORKDIR 指令可以来指定`工作目录`（或者称为`当前目录`），以后各层的当前 目录就被改为指定的目录，该目录需要已经存在， WORKDIR 并`不会`帮你建立目 录

  ```dockerfile
  RUN cd /app
  RUN echo "hello" > world.txt # 在 Dockerfile 中，这两行 RUN 命令的执行环境根本不同，是两个完全不同的容器
  # 每一个 RUN 都是启动一个容器、执行命令、然后提交存储层文件变更。第一层 RUN cd /app 的执行仅仅是当前进程的工作目录变更，一个内存上的变化而已，其结果不会造成任何文件变更。而到第二层的时候，启动的是一个全新的容器，跟第一层的容器更完全没关系，自然不可能继承前一层构建过程中的内存变化,
  # 因此如果需要改变以后各层的工作目录的位置，那么应该使用 WORKDIR 指令
  ```

- USER 指定当前用户

  - USER <用户名>
  - USER 指令和 WORKDIR 相似，都是改变环境状态并影响以后的层。 WORKDIR 是改变工作目录， USER 则是改变之后层的执行 RUN , CMD 以及 ENTRYPOINT 这类命令的身份。 
  - 当然，和 WORKDIR 一样， USER 只是帮助你切换到指定用户而已，这个用户必 须是事先建立好的，否则无法切换。

- HEALTHCHECK 健康检查

  - `HEALTHCHECK [选项] CMD <命令>` ：设置检查容器健康状况的命令 
  - `HEALTHCHECK NONE` ：如果基础镜像有健康检查指令，使用这行可以屏蔽掉 其健康检查指令

  - 当在一个镜像指定了 HEALTHCHECK 指令后，用其启动容器，初始状态会为 starting ，在 HEALTHCHECK 指令检查成功后变为 healthy ，如果连续一定 次数失败，则会变为 unhealthy 。
  - HEALTHCHECK 支持下列选项： 
    - `--interval=<间隔> `：两次健康检查的间隔，默认为 30 秒； 
    - `--timeout=<时长>` ：健康检查命令运行超时时间，如果超过这个时间，本次 健康检查就被视为失败，默认 30 秒； 
    - `--retries=<次数>` ：当连续失败指定次数后，则将容器状态视为 unhealthy ，默认 3 次。
  - 和 CMD , ENTRYPOINT 一样， HEALTHCHECK 只可以出现一次，如果写了多个， 只有最后一个生效。
  - 在 `HEALTHCHECK [选项] CMD` 后面的命令，格式和 `ENTRYPOINT` 一样，分为 `shell` 格式，和 `exec` 格式。命令的返回值决定了该次健康检查的成功与 否： 0 ：成功； 1 ：失败； 2 ：保留，不要使用这个值。

  ```dockerfile
  FROM nginx
  RUN apt-get update && apt-get install -y curl && rm -rf /var/lib
  /apt/lists/*
  HEALTHCHECK --interval=5s --timeout=3s \
  CMD curl -fs http://localhost/ || exit 1
  # 这里我们设置了每 5 秒检查一次（这里为了试验所以间隔非常短，实际应该相对较长）
  # 如果健康检查命令超过 3 秒没响应就视为失败，
  # 并且使用 curl -fs http://localhost/ || exit 1 作为健康检查命令
  ```

  - 如果健康检查连续失败超过了重试次数，状态就会变为 (`unhealthy`) 。 为了帮助排障，健康检查命令的输出（包括 `stdout` 以及 `stderr` ）都会被存储 于健康状态里，可以用 `docker inspect` 来查看。

  ```shell
   docker inspect --format '{{json .State.Health}}' web | python -m json.tool
  ```

- ONBUILD 为他人做嫁衣裳

  - `ONBUILD <其它指令>`
  - ONBUILD 是一个特殊的指令，它后面跟的是其它指令，比如 RUN , COPY 等， 而这些指令，在当前镜像构建时并不会被执行。只有当以当前镜像为基础镜像，去 构建下一级镜像的时候才会被执行。
  - Dockerfile 中的其它指令都是为了定制当前镜像而准备的，唯有 ONBUILD 是 为了帮助别人定制自己而准备的

#### 从 rootfs 压缩包导入 

> 格式： docker import [选项] <文件>||- [<仓库名>[:<标签>]] 

> 压缩包可以是本地文件、远程 Web 文件，甚至是从标准输入中得到。压缩包将会 在镜像 / 目录展开，并直接作为镜像第一层提交。

```shell
$ docker import \
http://download.openvz.org/template/precreated/ubuntu-14.04-
x86_64-minimal.tar.gz \
openvz/ubuntu:14.04
```

#### docker save 和 docker load 

> Docker 还提供了 docker load 和 docker save 命令，用以将镜像保存为一 个 tar 文件，然后传输到另一个位置上，再加载进来。这是在没有 Docker Registry 时的做法，现在已经不推荐，镜像迁移应该直接使用 Docker Registry，无 论是直接使用 Docker Hub 还是使用内网私有 Registry 都可以。

```shell
$ docker save alpine | gzip > alpine-latest.tar.gz
$ docker load -i alpine-latest.tar.gz
```

#### 删除本地镜像

>  并非所有的 docker rmi 都会产生删除镜像的行为，有可 能仅仅是取消了某个标签而已