## 镜像的实现原理

> Docker 镜像是怎么实现增量的修改和维护的？ 每个镜像都由很多层次构成， Docker 使用 **Union FS** 将这些不同的层结合到一个镜像中去。

- 通常 Union FS 有两个用途, 

  - 一方面可以实现不借助 LVM、RAID 将多个 disk 挂到 同一个目录下,
  - 另一个更常用的就是将一个只读的分支和一个可写的分支联合在一 起，Live CD 正是基于此方法可以允许在镜像不变的基础上允许用户在其上进行一 些写操作。

  Docker 在 AUFS 上构建的容器也是利用了类似的原理。

## 操作 Docker 容器

> 简单的说，容器是`独立运行的一个或一组应用，以及它们的运行态环境`。对应的， 虚拟机可以理解为`模拟运行的一整套操作系统（提供了运行态环境和其他系统环境）和跑在上面的应用`。

- 当利用 docker run 来创建容器时，Docker 在后台运行的标准操作包括： 
  - 检查本地是否存在指定的镜像，不存在就从公有仓库下载 
  - 利用镜像创建并启动一个容器 
  - 分配一个文件系统，并在只读的镜像层外面挂载一层可读写层 
  - 从宿主主机配置的网桥接口中桥接一个虚拟接口到容器中去 
  - 从地址池配置一个 ip 地址给容器 
  - 执行用户指定的应用程序 
  - 执行完毕后容器被终止

#### 后台(background)运行

> 可以通过添加 `-d` 参数来实现,  此时容器会在后台运行并不会把输出的结果(STDOUT)打印到宿主机上面(输出结果 可以用docker logs 查看)

- 使用 -d 参数启动后会返回一个唯一的 id，也可以通过 docker ps 命令来查看 容器信息

#### 终止容器

> docker stop

- `docker restart` 命令会将一个运行态的容器终止，然后再重新启动它。

#### 进入容器

> attach 命令 `docker attach` 是Docker自带的命令

```shell
$sudo docker attach nostalgic_hypatia
```

- 但是使用 attach 命令有时候并不方便。当多个窗口同时 attach 到同一个容器的时候，所有窗口都会同步显示。当某个窗口因命令阻塞时,其他窗口也无法执行操作 了。

#### 导出和导入容器 

##### 导出容器 

- 如果要导出本地某个容器，可以使用 `docker export` 命令。 

```shell
$ sudo docker ps -a 
CONTAINER ID IMAGE COMMAND CREA TED STATUS PORTS NA MES 7691a814370e ubuntu:14.04 "/bin/bash" 36 h ours ago Exited (0) 21 hours ago te st 
$ sudo docker export 7691a814370e > ubuntu.tar  # 将导出容器快照到本地文件
```

##### 导入容器快照 

> 可以使用 `docker import` 从容器快照文件中再导入为镜像，例如 

```shell
$ cat ubuntu.tar | sudo docker import - test/ubuntu:v1.0 
$ sudo docker images 
REPOSITORY TAG IMAGE ID CREA TED VIRTUAL SIZE 
test/ubuntu v1.0 9d37a6082e97 Abou t a minute ago 171.3 MB 
```

此外，也可以通过指定 URL 或者某个目录来导入，例如 

```shell
$sudo docker import http://example.com/exampleimage.tgz example/ imagerepo
```

`*注：用户既可以使用 docker load 来导入镜像存储文件到本地镜像库，也可以 使用 docker import 来导入一个容器快照到本地镜像库。这两者的区别在于容 器快照文件将丢弃所有的历史记录和元数据信息（即仅保存容器当时的快照状 态），而镜像存储文件将保存完整记录，体积也要大。此外，从容器快照文件导入 时可以重新指定标签等元数据信息。`

#### 删除容器

可以使用 docker rm 来删除一个处于终止状态的容器。 例如 

```shell
$sudo docker rm trusting_newton trusting_newton 
```

如果要删除一个运行中的容器，可以添加 -f 参数。Docker 会发送 SIGKILL 信号给容器。 

##### 清理所有处于终止状态的容器 

用 `docker ps -a` 命令可以查看所有已经创建的包括终止状态的容器，如果数量 太多要一个个删除可能会很麻烦，用 `docker rm $(docker ps -a -q)` 可以全 部清理掉。 

`*注意：这个命令其实会试图删除所有的包括还在运行中的容器，不过就像上面提过 的 docker rm 默认并不会删除运行中的容器。`









