### Docker mongodb

=================本地路径映射到docker容器路径下====
#### 下载 mongodb
docker pull mongo

#### 运行容器
docker run --name mongo -p 27017:27017  -d mongo -v D:\DockerWorkspace\mongo\db:/data/db #（此处似有bug不能映射数据目录）
     ====>若启动失败执行：
docker run --name mongo -p 27017:27017  -d mongo

#### 进入容器操作

docker exec -it container_id   /bin/bash


#### mongo操作创建管理账号
mongo  

use admin

db.createUser({user:"root",pwd:"root",roles:[{role:'root',db:'admin'}]})   //创建用户,此用户创建成功,则后续操作都需要用户认证

exit  

#测试管理账号
 mongo 127.0.0.1/admin -uroot -p

==mongodb默认不需要用户认证!