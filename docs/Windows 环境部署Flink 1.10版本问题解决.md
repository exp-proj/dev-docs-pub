#### 【Flink】Windows环境部署Flink 1.10版本问题解决

1. 下载 http://archive.apache.org/dist/flink/flink-1.10.0/flink-1.10.0-bin-scala_2.11.tgz   
2.  解压到自定义目录
3. 双击 `F:\flink-1.10.0\bin\start-cluster.bat`
4. 进入 web 页面(http://localhost:8081/) 查看
5. 在Windows环境部署Flink 1.10版本，发现TaskManager启动失败，看日志主要是几个内存参数没有配置导致的，简单记录一下。

直接通过start-cluster.bat启动会拉起JobManager和TaskManager，如果下列参数不配置的话，TaskManager进程会退出，日志显示相关的参数没有配置，在conf/flink-conf.yaml中添加下面的配置，重新启动即可：

```yml
taskmanager.cpu.cores: 2
taskmanager.memory.task.heap.size: 512m
taskmanager.memory.managed.size: 512m
taskmanager.memory.network.min: 64m
taskmanager.memory.network.max: 64m
```

参数可以根据自己的需要设置。

