## CentOS 7 安装 Jenkins

> gitlab java maven 持续集成

- 首先需要安装 JAVA, MAVEN 环境

- 如果你的系统没有自带git，那么也需要安装一个

  ```shell
  yum install git
  ```

- 安装

  - 第一种方法（应该是自动获取最新版本）

    ```shell
    sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo
    sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
    
    yum install jenkins
    ```

  - 第二种方法（自行指定安装版本）

    直接下载 rpm 安装

    各个版本地址 https://pkg.jenkins.io/

    ```shell
    wget https://pkg.jenkins.io/redhat/jenkins-2.156-1.1.noarch.rpm
    rpm -ivh jenkins-2.156-1.1.noarch.rpm
    ```

- 配置

  ```shell
  vim /etc/sysconfig/jenkins
  
  #监听端口(默认的是 8080)
  JENKINS_PORT="8080"
  ```

- 配置权限

  `为了不因为权限出现各种问题，这里直接使用root`

  ```shell
  vim /etc/sysconfig/jenkins
  
  #修改配置(默认的是 jenkins)
  $JENKINS_USER="root"
  ```

- 重启

  ```shell
  service jenkins restart
  ps -ef | grep jenkins (查看是否在运行)
  ```

- 启动

  ```shell
  systemctl start jenkins
  ```

- 初始访问

  - 访问jenkins地址 http:<ip或者域名>:8080

  ![1531204667345](./picture/668104-20180710201226584-1112601029.png)

  - 执行命令查看密码：

    ```shell
    cat /var/lib/jenkins/secrets/initialAdminPassword
    ```

  - 插件安装选择推荐插件(测试时机器配置太差，有的没装成功，但是不影响使用，后边会把需要的插件再安装一下)

    ![1531204844660](./picture/668104-20180710201226138-1190837537.png) 

  - 安装进行中

    ![1531204864191](./picture/668104-20180710201225629-148350366.png)

  - 插件安装完成以后将会创建管理员账户

    ![1531205120250](./picture/668104-20180710201225150-820617266.png)

  - 安装完成：

    ![1531205170165](./picture/668104-20180710201224689-1468094918.png)

- 基本的安装配置就到这里了。

### git + maven 打包配置过程

- #### 配置 maven 项目

  - **系统管理** -> **插件管理** 

    ![image-20200905101202435](./picture/image-20200905101202435.png)

    ![image-20200905101321359](./picture/image-20200905101321359.png)

    ![image-20200905101421631](./picture/image-20200905101421631.png)

  - 最初新建任务时 是不能新建 maven 项目的，需要安装 maven 插件后才会出现

  ![img](./picture/webp01)

  - 安装maven插件： Maven Integration、Pipeline Maven Integration

    **系统管理** -> **插件管理** 

    ![image-20200905095703037](./picture/image-20200905095703037.png)

  - 安装成功之后再去尝试构建新任务就会出现**Maven项目**

- #### 配置 gitlab 

  - 安装 Jenkins 的服务器要配置 gitlab 能够 获取 gitlab 上的代码，具体配置这里不详述。这里主要配置 Jenkins

  - 安装相关插件：Gitlab Hook、Gitlab Authentication、Gitlab、Git Paramete、Publish Over SSH、Build Authorization Token Root ; 安装方式跟上边 maven 的一样

  - **系统管理** -> **系统配置** (这一步还没搞清楚是干啥的)

    - 配上安装 Jenkins 服务器的密钥，

    ![image-20200905102840411](./picture/image-20200905102840411.png)

    - 配置安装 Jenkins 服务器的IP

    ![image-20200905103247188](./picture/image-20200905103247188.png)

  

- #### 现在就可以创建maven 任务了

  - **首页** -> **新建任务**

  ![image-20200905103553791](./picture/image-20200905103553791.png)

    - 确定后进入项目配置，

      - 首先写描述（建议还是写一些）
        ![image-20200905103937641](./picture/image-20200905103937641.png)

      - 要构建的项目的 gitlab 的ssh 链接, 

        - Credentials:（可以在这里添加，应该也是可以在全局配置里设置的）

          ![image-20200905104732441](./picture/image-20200905104732441.png)

          - Passphrase 把密钥粘上

          ![image-20200905104852559](./picture/image-20200905104852559.png)

        ![image-20200905104458801](./picture/image-20200905104458801.png)

        - 构建触发器

          ![image-20200905105220321](./picture/image-20200905105220321.png)

      - 这里还需要配置 gitlab push; Jenkins自动触发

        ![image-20200905174825078](./picture/image-20200905174825078.png)

        ![image-20200905174927501](./picture/image-20200905174927501.png)

        - 后边的打包，之后根据需要配置即可

            ![image-20200905175936303](./picture/image-20200905175936303.png)

      

      - 把上边构建触发器步骤中的 http://... 链接及 Secret token 粘到 gitlab 对应项目的下图位置中：

        保存后，点击测试，成功则会出现下图提示，再到 Jenkins 上查看，任务已在打包

      ![image-20200905175345914](./picture/image-20200905175345914.png)

      ![](./picture/image-20200905175622853.png)

      

      

      

      

      

      

      

      - 点击**Add webhook**, 该页面的最下边会有添加的一条 `Project Hooks`, 点击**Test** , 成功则会有下图提示，再到 Jenkins 查看，该项目任务正在进行中

      ![image-20200905175622853](./picture/image-20200905175622853.png)



