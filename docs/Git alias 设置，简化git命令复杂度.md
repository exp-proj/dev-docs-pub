# git alias设置，简化git命令复杂度

0. 默认本地已安装git工具（windows）
1. 命令 cd ~ ，进入根目录，
2. 命令 ls -la | grep .gitconfig，找到根目录下的 .gitdionfig文件
3. - vi .gitconfig 修改这个文件的配置。
   - git config --globaluser.name"你的名字"
   - git config--globaluser.email"你的Email"
4. 简化git执行命令的方法一

    ```shell
    git config --global alias.co checkout
    ```

    ```shell
    git config --global alias.unstage 'reset HEAD'
    ```

    命令解释：

    - 在 `alias.` 后面加上你想要的别名，之后再打上一个空格，再加上你想要简化命令的原始名称。

    - 配置 `git` 的时候加上 `global` 这个参数，代表的是这个命令对当前用户有效。如果不加的话，仅仅代表对这个仓库有效。

5. 修改 `~/.gitconfig` 文件

```shell
[user]
    name = chenkai@vpgame.cn
    email = chenkai@vpgame.cn
[alias]
	st = status
	co = checkout
	ci = commit
	br = branch
	unstage = reset HEAD
	last = log -1
	lg = log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset ' --abbrev-commit
	ps = push
	pl = pull
```

