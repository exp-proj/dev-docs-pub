## Linux操作命令录制神器-Asciinema

## Asciinema 简介

`Asciinema` 是一款开源免费的终端录制工具，它可以将命令行输入输出的任何内容加上时间保存在文件中，同时还提供方法在终端或者web浏览器中进行回放。在播放过程中你随时可以暂停，然后对`播放器`中的文本进行`复制`或者`其它操作`。`官方网址` https://asciinema.org

## 怎么运作

当您`asciinema rec`在终端中运行时，记录开始，捕获您在发出shell命令时正在打印到终端的所有输出。录制完成时（通过敲击`Ctrl-D`或键入`exit`），然后将捕获的输出上传到`asciinema.org网站`或者通过`asciinema命令播放`。相比GIF和视频文件体积非常之小，无需缓冲播放。

asciinema由以下三个子项目构成：

- asciinema：基于命令行的终端会话记录器
- asciinema.org：提供API供上传录像和展示的网站
- javascript player：用于在web上播放录像的js播放器

## 支持平台及安装

- MAC

```
brew install asciinema
```

- Pip安装

```
sudo pip3 install asciinema
```

- CentOS or RedHat

```
yum install asciinema
```

- Arch Linux

```
pacman -S asciinema
```

- Debian

```
sudo apt-get install asciinema
```

- Ubuntu

```
sudo apt-add-repository ppa:zanchey/asciinemasudo apt-get updatesudo apt-get install asciinema
```

很遗憾，不支持`Windows`安装

## 参数

| 参数        | 解释                                                   |
| ----------- | ------------------------------------------------------ |
| --stdin     | 启用标准输入录制                                       |
| --append    | 添加录制到已存在的文件中                               |
| --raw       | 保存原始STDOUT输出，无需定时信息等                     |
| --overwrite | 如果文件已存在，则覆盖                                 |
| -c          | 要记录的命令，默认为$SHELL                             |
| -e          | 要捕获的环境变量列表，默认为SHELL,TERM                 |
| -t          | 后跟数字，指定录像的title                              |
| -i          | 后跟数字，设置录制时记录的最大空闲时间                 |
| -y          | 所有提示都输入yes                                      |
| -q          | 静默模式，加了此参数在进入录制或者退出录制时都没有提示 |
| -s          | 后边跟数字，表示用几倍的速度来播放录像                 |
| -i          | 后边跟数字，表示在播放录像时空闲时间的最大秒数         |

```
asciinema推荐`的文件后缀是`.cast`，输入`exit`或按`ctrl+D`组合键`退出录制
```

## 录制

```
$ asciinema rec operation.cast
```

## 播放

```
$ asciinema play operation.cast
```