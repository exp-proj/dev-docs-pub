## Docker获取容器(MySQL,Redis等)内部IP地址

1. 使用docker inspect命令获取容器/镜像的元数据。

```
docker inspect 容器id 
```

![img](./picture/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM5NTA2OTEy)

>  这里我们使用第一条命令查看mysql容器的元数据。箭头所指处则为mysql容器当前的内部ip地址，**部署web项目时，在使用jdbc连接至mysql时需要填写此ip地址而不是localhost。**

2. 如果你觉得元数据太复杂无法寻找的话，可以使用以下命令获得指定容器id的ip，或所有容器的ip。

```shell
//获取指定容器的ip
docker inspect --format '{{ .NetworkSettings.IPAddress }}' 68f0d84be6ad

//获取所有容器ip
docker inspect --format='{{.Name}} - {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps -aq)
```

![img](./picture/NzZG4ubmV0L3FxXzM5NTA2OTEy)