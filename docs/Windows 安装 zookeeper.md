#### Windows 安装 zookeeper

1. 下载

http://archive.apache.org/dist/zookeeper/zookeeper-3.4.9/zookeeper-3.4.9.tar.gz   

2. 解压到 `F:\zookeeper-3.4.9`

3. 修改`F:\zookeeper-3.4.9\conf` 目录下`zoo_sample.cfg`为`zoo.cfg`

4. 配置`zoo.cfg` 中 `dataDir=F:/log/zookeeper`

5. 添加zookeeper的环境变量

   - 在**系统变量**中添加

   `ZOOKEEPER_HOME = F:\zookeeper-3.4.9`

   - 编辑**path系统变量**，添加路径

   `%ZOOKEEPER_HOME%\bin`

6. 单机模式
   打开一个新的cmd命令窗口，执行命令 `zkServer`

