## Java 如何优雅的实现 try/catch 异常块？

在项目中，我们会遇到异常处理，对于运行时异常，需要我们自己判断处理。对于受检异常，需要我们主动处理。

但是繁琐的try{}caht嵌套在代码里，看着很不舒服，这里我们不讨论性能，就代码来讲，来看看如何将他隐藏起来。原理是不变的。变得是写法。下面我们来看如何优雅的处理异常块。

在这之前。你需要知道以下几个概念：

- 行为参数化：

- - 是java8提出的，函数式编程的一种思想，通过把代码包装为参数传递行为，即把代码逻辑包装为一个参数，传到方法里。

- Lambda表达式:

- - java8提出：Lambda表达式理解为简洁的表示可传递的匿名函数的一种方式，它没有名称，但它有函数体，参数列表，返回类型。可以抛出一个异常类型。包装代码逻辑为参数即使用Lambda表达式。

- 函数式接口：

- - 本质上是只有一个抽象方法的普通接口，可以被隐式的转换为Lambda表达式，需要用注解定义(@FunctionalInterface)。默认方法和静态方法可以不属于抽象方法，可以在函数式接口中定义。

![img](https://mmbiz.qpic.cn/mmbiz_png/6mychickmupW2NMkkdJCO9I4G7KyvmPE9CJuWkPe3cQVibh7vGe7gSuDRl4ibbjEnWV6MMKwAdPMT4hM9BpCsicXgw/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

如果函数式接口中额外定义多个抽象方法，那么这些抽象方法签名必须和Object的public方法一样,接口最终有确定的类实现， 而类的最终父类是Object。因此函数式接口可以定义Object的public方法。

![img](https://mmbiz.qpic.cn/mmbiz_png/6mychickmupW2NMkkdJCO9I4G7KyvmPE9DEX3dGI44UYhjsyCMgEufNtROj5ibkRY5tYpa3yHbweD8qrtAzofYRQ/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

这句代码想来小伙伴都不陌生。这是一个受检异常，需要抛出一个ClassNotFoundException。

正常的写法：

![img](https://mmbiz.qpic.cn/mmbiz_png/6mychickmupW2NMkkdJCO9I4G7KyvmPE9QqRBeibuXib3icfhc4gYg0gRhG5kOD04DHPmZbtg2EYDwicwJ5m42KoO7g/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

![img](https://mmbiz.qpic.cn/mmbiz_png/6mychickmupW2NMkkdJCO9I4G7KyvmPE9W5f5IbstLUW79zQ3NWCAcBzxfA3iaL4qrORIaB8IkoCY4JzenpQq73Q/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

嗯，我们来看具体的实现：很简单，我们要做的，即把`Class<?> clazz = Class.forName("类名");`当做一种行为去处理，接受一个String ，得到一个Class，所以我们要定义一个函数接口，描述这种行为。

![img](https://mmbiz.qpic.cn/mmbiz_png/6mychickmupW2NMkkdJCO9I4G7KyvmPE9fQCmOpvqphbZdkH4Xibo6RRibTfu6lAoPs9mRY4ibQmZOMe2ociapODpog/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

这里，因为我们的行为需要抛出异常。所以在接口里也抛出异常。

然后，我们需要定义一个方法，将我们的行为作为参数传进去，同时，捕获一下我们的异常。

![img](https://mmbiz.qpic.cn/mmbiz_png/6mychickmupW2NMkkdJCO9I4G7KyvmPE9Kd0kfUcJEqY4cPNm4xbwXGL5tymLM7uf96qyE1edA3BRq020ZM94fg/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

然后，我们可以调用我们的方法classFind方法，

![img](https://mmbiz.qpic.cn/mmbiz_png/6mychickmupW2NMkkdJCO9I4G7KyvmPE9l0LIcAeicCJfJU2YG5J3Qd6DgTQtQB4T89mYicErFQUw0wKcentXiaSyQ/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

当然。其实这种思想并不简单的可以做捕获异常的处理，我们来看一个Demo->文本文件转换为字符串：

在我看来；将文本文件转换为字符串，我们需要使用高级流包装低级流，然后做缓存读出来。这里，我们不可避免的会遇到异常处理，流的关闭等操作，下面我们将这些代码都异常起来。专心写读的逻辑即可。

我的思路：

我对java IO用的不是很熟，大家有好的方法请留言，相互学习：

```
FileInputStream fileInputStream = new FileInputStream(file))

InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream))

BufferedReader bufferedReader = new BufferedReader(inputStreamReader))

String str = bufferedReader.readLine()
```

字节流-》字符流-》字符缓存流 即 将字节流转换为字符流之后在用高级流包装。

所以我的思路是避免在逻辑里出现太多的IO流关闭，和异常捕获，专心处理读取逻辑即可，结合以下两种技术：

- 

- - try(){}【自动关闭流，1.7支持】

- 

- - lambda特性来实现【行为参数化，1.8】

![img](https://mmbiz.qpic.cn/mmbiz_png/6mychickmupW2NMkkdJCO9I4G7KyvmPE9xviaHvu5kfXtcI354MAv5gjn7iaXMZZMtlkicEOeX4m0M7TngiawDLUibpA/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

执一个行为，任何BufferReader -> String的Lambda表达式都可以作为参数传入。只要符合peocess方法的签名即可。

![img](https://mmbiz.qpic.cn/mmbiz_png/6mychickmupW2NMkkdJCO9I4G7KyvmPE9NkfUptWeFQY6n7dfAOe0iaZGDs4ml4t65Q1ec16DE6wCaL4Y3VXMwPA/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

执行

![img](https://mmbiz.qpic.cn/mmbiz_png/6mychickmupW2NMkkdJCO9I4G7KyvmPE9ZHYxeSvZ6x6goPgic74uc7B9JK513cY72eTPaqiaFAYSOcia0BryRN8tw/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)