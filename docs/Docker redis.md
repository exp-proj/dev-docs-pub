#### Docker redis 

=================本地路径映射到docker容器路径下====
##### 下载 redis
```
docker pull redis
```

##### 运行容器:

```
docker run -p 6379:6379 -v /d/DockerWorkspace/redis/data:/data  -d redis redis-server --appendonly yes
```

##### 连接进入容器

```
docker exec -it container_id redis-cli
```

##### 进行简单的测试
127.0.0.1:6379> set ckai "ckai"
OK
127.0.0.1:6379> get ckai
"ckai"

#### Redis 可视化工具：

https://github.com/caoxinyu/RedisClient

