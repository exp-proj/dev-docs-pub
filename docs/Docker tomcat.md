### Docker tomcat

#### 下载tomcat
docker pull tomcat

#### 启动容器

docker run --name tomcat -p 8080:8080 -v D:\DockerWorkspace\tomcat\Web:/usr/local/tomcat/webapps/test -d tomcat  

#### 访问http://host:8080/test进行验证,具体需不需要别的子目录可以直接在webapps这一级映射路径.