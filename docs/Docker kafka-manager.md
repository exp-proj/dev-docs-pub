### Docker:kafka-manager

DockerHub：https://hub.docker.com/r/sheepkiller/kafka-manager

```shell
docker run -it \
	--name kafka-manager \
	-p 9000:9000 \
	-e ZK_HOSTS=127.0.0.1:2181 \ # ip 需要跟zookeeper host 相同(用外网IP)
	-e KAFKA_MANAGER_AUTH_ENABLED=true \
	-e KAFKA_MANAGER_USERNAME=admin \
	-e KAFKA_MANAGER_PASSWORD=admin \
	-d sheepkiller/kafka-manager:latest
```

### 配置 kafka-manager

kafka-manager 默认的端口是 9000。

1. 添加集群

   ![1597386588682](./picture/1597386588682.png)

2. 配置集群

   ![1597386711921](./picture/1597386711921.png)

3. 开启消费者查看

   ![1599558709720](./picture/1599558709720.png)

###  https://jueee.github.io/2020/08/2020-08-14-kafka-manager%E7%9A%84%E5%AE%89%E8%A3%85%E5%92%8C%E4%BD%BF%E7%94%A8/

