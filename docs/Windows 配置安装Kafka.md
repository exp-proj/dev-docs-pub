#### Windows 配置安装Kafka

1. 下载

 `http://archive.apache.org/dist/kafka/2.2.0/kafka_2.12-2.2.0.tgz `

2. 解压到 `F:\kafka_2.12-2.2.0`

3. 配置F

   - 修改配置文件`config/server.properties`下面两个参数值；并在F盘建立目录`F:/kafka/logs`

   ```properties
   log.dirs=F:/log/kafka
   zookeeper.connect=localhost:2181
   ```

4. 编辑config文件夹下的zookeeper.properties

   - `dataDir=F:/log/zookeeper-kafka`
   
5. 运行

   - 启动 zookeeper  `zkServer`
   - 启动 kafka `.\bin\windows\kafka-server-start.bat .\config\server.properties` (这里的.\config\server.properties一定要加上)
   - 创建一个主题，并用客户端发送消息

   ```shell
   .\bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic abc
   
   .\bin\windows\kafka-console-producer.bat --broker-list localhost:9092 --topic abc
   ```

   - 启动消费者，可以看到客户端传来的消息。此时，在客户端输入的消息都将传到消费者端。

   ```shell
   .\bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic abc --from-beginning
   ```

   > 注： 新版本的消费者命令有所改变