### Docker nginx

==============本地路径映射到docker容器路径下====
#### 下载 nginx
docker pull nginx

#### 运行容器：
docker run -p 90:80 --name nginx -v D:\DockerWorkspace\nginx\Web:/usr/share/nginx/html -v D:\DockerWorkspace\nginx\nginx.conf:/etc/nginx/nginx.conf -v D:\DockerWorkspace\nginx\logs:/var/log/nginx  -d nginx

#注意 本地需要nginx.conf配置

#### nginx内部对应的html地址位于/usr/share/nginx/html

#### nginx内部对应的配置文件地址位于/etc/nginx/nginx.conf

#### 下面是宿主机上nginx.conf的配置内容

```json
worker_processes  1;
events {
    worker_connections  128;
}
http {
    include       mime.types;
    default_type  application/octet-stream;
    sendfile        on;
    keepalive_timeout  65;

  server {
​        listen       80;
​        server_name  localhost;
​        location / {
​            #替换下面的地址为您网页目录地址
​            root   /usr/share/nginx/html;
​            index  index.html index.htm;
​        }
​        error_page   500 502 503 504  /50x.html;
​        location = /50x.html {
​            root   html;
​        }
​    }
}
```


