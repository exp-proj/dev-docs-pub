# CentOS7 下安装rpm jdk1.8 和 maven

#### 【1】查看并卸载自带的openjdk

- 查看系统中默认安装的jdk：

```shell
rpm -qa|grep jdk
```

- 卸载JDK相关文件:

```shell
yum -y remove java-1.7.0-openjdk* # “*”表示卸载掉java 1.7.0的所有openjdk相关文件。
```

- 或者如下卸载jdk：

```shell
yum -y remove java-1.8.0-openjdk-headless-1.8.0.65-3.b17.el7.x86_64
```

#### 【2】下载并安装jdk1.8.0_144 

- 直接使用wget命令在linux服务器上下载：

```shell
wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" https://download.oracle.com/otn-pub/java/jdk/8u201-b09/42970487e3af4f5aa5bca3f542482c60/jdk-8u201-linux-x64.rpm

or

wget --no-cookies --no-check-certificate --header "Cookie: oraclelicense=accept-securebackup-cookie" https://download.oracle.com/otn-pub/java/jdk/8u201-b09/42970487e3af4f5aa5bca3f542482c60/jdk-8u201-linux-x64.rpm
```

- 安装rpm包：

```shell
rpm -ivh jdk-8u201-linux-x64.rpm
```

- 查看系统默认java版本

```shell
java -version
```

![img](./picture/aHR0cDovL2ltZy5ibG9nLmNzZG4ubmV0LzIwMTcwOTI5MTQyMzM4NDMz)

说明安装成功。

- 修改profile文件

```shell
vim /etc/profile
```

- 在文件最后追加

```shell
export JAVA_HOME=/usr/java/jdk1.8.0_201
export PATH=$PATH:$JAVA_HOME/bin:$JAVA_HOME/jre/bin
export CLASSPATH=$JAVA_HOME/lib:$JAVA_HOME/lib/tools.jar
```

- 保存退出，使其生效：

```shell
source /etc/profile
```





### maven 安装

- 下载压缩包

```shell
wget https://mirror-hk.koddos.net/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz
```

- 解压 

```shell
tar -xvzf apache-maven-3.6.3-bin.tar.gz
```

- 配置环境变量

```shell
vim /etc/profile

# 在文件最后追加
export MAVEN_HOME=/mnt/maven/apache-maven-3.6.3
export PATH=${PATH}:$JAVA_HOME/bin:$JAVA_HOME/jre/bin:${MAVEN_HOME}/bin
```

- 确认maven安装成功

```shell
mvn -v
```

