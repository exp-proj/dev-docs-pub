### 小技巧、命令

```shell
netstat -tunlp | egrep "(2181|9092)" // zookeeper kafka
```

```shell
hadoop fs -du -h /user/hive/warehouse/ods.db/ods_test
```

```shell
data=$(hadoop fs -du -h /user/hive/warehouse/ods.db/ods_test |tail -1)
echo ${data:0-10:10}

data=$(spark-sql -e "show partitions ods.ods_test" |tail -1)
echo ${data:3}
```

```shell
nohup sh /home/ck/scripts/sh_scripts_batch.sh /mnt/scripts/ods/card/update.sh 20201126 20201129 >> /home/ck/card.log 2>&1 &
```

```shell
cut -d : -f 1 /etc/passwd # 查询 是否有该用户 
```

```shell
groupadd docker # 查询 是否存在docker组
```

```shell
gpasswd -a gitlab-runner docker # 将gitlab-runner加入docker组
```

```shell
hadoop fs -rm -skipTrash /analytics/log_eps/2018-01 # 不通过回收站删除文件
```

// 返回： 当前正在执行和已经提交的任务

> http://ip-10-108-8-231.ec2.internal:8088/ws/v1/cluster/apps?state=RUNNING,ACCEPTED

// 返回： flink job_ID 和 运行状态

> http://ip-10-108-8-231.ec2.internal:20888/proxy/application_1615446662766_0010/jobs

// 返回： flink job Configuration

> http://ip-10-108-8-231.ec2.internal:20888/proxy/application_1615446662766_0010/config

// 返回： flink job checkpoints 信息

> http://ip-10-108-8-231.ec2.internal:20888/proxy/application_1615446662766_0010/jobs/cf88bc86de496211e98ca55d7ee160fc/checkpoints

// 返回： flink job checkpoints 配置

> http://ip-10-108-8-231.ec2.internal:20888/proxy/application_1615446662766_0010/jobs/cf88bc86de496211e98ca55d7ee160fc/checkpoints/config

// Flink 官网可视化工具 visualizer（可以将执行计划 JSON 绘制出执行图）

https://flink.apache.org/visualizer/

// Notepad++缓存文件的位置

```shell
C:\Users\USER\AppData\Roaming\Notepad++\backup
```

```shel
%AppData%\Notepad++\backup
```











