windows系统配置Protobuf:
1.protobuf下载：所有版本 https://github.com/protocolbuffers/protobuf/releases/tag/v3.6.1
	我下载的是： protoc-3.6.1-win32.zip 目前没有win64位的。
2.解压protoc-3.6.1-win32.zip（目录随意）；
3.在系统环境变量下配置 ;D:\workSpace\protoc-3.6.1-win32\bin  目的：指向protoc.exe
4.校验： windows+R, cmd, 小黑屏输入 protoc 或 protoc --version,显示版本则安装完成；该过程没遇见问题，如果有问题应该是系统环境变量的配置有问题；
5.添加使用语言的插件，java不需要，直接编译就行了；
					go需要 protoc-gen-go.exe；
					grpc需要protoc-gen-grpc-gateway；
	插件获取方式：如果能翻墙可以直接执行：
					go get -u github.com/golang/protobuf/protoc-gen-go
					go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
					go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
				不能翻墙就要自己build:
					protoc-gen-go ： 
								在github.com\golang\protobuf\protoc-gen-go,
								 在该目录下直接执行 go build;
								 没报错就会有 protoc-gen-go.exe，可以直接将其复制到 %gopath%\bin 目录下；
								 报错一般会是某些包找不到，这个时候就要到github上一一对应的下载go的依赖包，如：google.golang.org、github.com、golang.org，包要放在%gopath%\src目录下，由于go库还在不断完善有些包目录会有差异，所以目录名称一定要对；直到build成功；
					protoc-gen-grpc-gateway ： 
								下载：https://github.com/grpc-ecosystem/grpc-gateway
								在 grpc-gateway\protoc-gen-grpc-gateway 目录下执行go build;
								没报错最好，有 protoc-gen-grpc-gateway.exe，同样直接将其复制到 %gopath%\bin 目录下；
								报错跟上边一样去github上下载对应包就行，直到build成功；这个下载的包比较多；如：google.golang.org、github.com、golang.org；
6.编译：
go 和 grpc 的编译： 可以一条命令同时编译这两个文件；
protoc -I protos\definitions //需要编译的proto文件目录
-I "%GOPATH%\src\github.com\grpc-ecosystem\grpc-gateway\third_party\googleapis" // 
-I %GOPATH%\src //可以不要
api.proto // 需要编译的proto文件
--plugin=protoc-gen-grpc-gateway=%GOPATH%\bin\protoc-gen-grpc-gateway.exe // 对应的插件
--grpc-gateway_out=logtostderr=true,grpc_api_configuration=.\api.yml:protos\gens  // logtostderr=true:如果编译过程出错，会有日志；grpc_api_configuration=.\api.yml : 相当于API文档；protos\gens ： 编译文件目录
--plugin=protoc-gen-go=%GOPATH%\bin\protoc-gen-go.exe // 可以同时添加多个插件，生成多个文件
--go_out=protos/gens // go语言的编译后文件目录
--java_out=C:\Users\ly\Desktop\VPGame

java 编译会生成文件夹，目录为.proto对应的package;
protoc -I=D:\workSpace\goworkspace\src\git.vpgame.cn\sh-team\dota2-analysis-result-data-api\protos\definitions // 输入目录
--java_out=C:\Users\ly\Desktop\VPGame // 输出目录
api.proto //待编译文件

over.

