## Python Kafka scripts

- 安装依赖模块

```shell
pip3 install kafka-python
```

- 生产者脚本

```python
# -*- coding:utf-8 -*-
from kafka import KafkaProducer
message = b"{\"database\":\"themis_eos\",\"table\":\"common_goods_storage\",\"type\":\"update\",\"ts\":1604370940,\"xid\":3153218,\"commit\":true,\"primary_key\":[38],\"primary_key_columns\":[\"id\"],\"data\":{\"id\":38,\"goods_id\":167476,\"storage\":23,\"storage_type\":0,\"is_delete\":0,\"create_time\":\"2020-11-03 02:25:57\",\"last_update_time\":\"2020-11-03 02:35:40\"},\"old\":{\"is_delete\":0,\"last_update_time\":\"2020-11-03 02:33:37\"}}"
producer = KafkaProducer(bootstrap_servers=['192.168.0.666:9092'])
for i in range(0, 1):
    producer.send('eos_maxwell', value=message, key=None, headers=None, partition=None, timestamp_ms=None)

producer.close()
```

- 消费者脚本

```python
from kafka import KafkaConsumer
import time

def log(str):
    t = time.strftime(r"%Y-%m-%d_%H-%M-%S",time.localtime())
    print("[%s]%s"%(t,str))

log('start consumer')
consumer=KafkaConsumer('eos_maxwell',group_id='test',bootstrap_servers=['192.168.0.666:9092'])
for msg in consumer:
    recv = "%s:%d:%d: key=%s value=%s" %(msg.topic,msg.partition,msg.offset,msg.key,msg.value)
    log(recv)

```

