**docker-compose是什么？**

  docker-compose是用来定义和运行多容器Docker应用程序的工具。

**准备阶段**

1. 已经安装docker和docker-compose。
2. 已经制作了web1和web2镜像并推送到远程仓库。

**编写docker-compose文件,并进行服务编排**

1. image：就是你的docker镜像
2. depends_on：代表web2本身依赖于web1，所以web1的启动要先于web2服务
3. volumes：可以对应 docker 操作中的 -v home/path/:/docker/path

```yaml
version: '1'
services:
     web1:
        image: docker.shj.com/shj/web1:v1
        ports:
          - "8088:8080"
        volumes:
          - /home/logs/:/usr/local/tomcat/logs/
          - /home/logs/:/home/logs/
     web2:
        image: docker.shj.com/shj/web2:v1
        depends_on:
          - web1
        volumes:
          - /home/logs/:/usr/local/tomcat/logs/
          - /home/logs/:/home/logs/
        ports:
          - "8089:8080"
     mysql: 
        environment:
            MYSQL_ROOT_PASSWORD: "shj@123"
            MYSQL_USER: "root"
            MYSQL_PASS: "shj@123"
        image: docker.shj.com/ops/mysql:5.7
        ports: 
           - "33306:3306"
     nginx:
        image: docker.shj.com/ops/nginx:v1
        volumes:
           - /home/logs/nginx/:/home/logs/nginx/
           - /home/shj/nginx/nginx_ssl/:/home/nginx/nginx/conf/cert/ssl/
           - /home/shj/nginx/nginx_conf/:/usr/local/nginx/conf/http_vhost/
           - /home/shj/web/web2/:/home/shj/apps/web/web2/
           - /home/shj/web/web1/:/home/shj/apps/web/web1/
        ports:
           - "80:80"
```

**总结**

  docker-compose适合于需要多个容器相互配合来完成服务的运行，当我们在工作中遇到一个项目运行，需要多个服务配合甚至数据库、负载均衡等这个时候我们可以考虑使用Compose编排管理，提高部署效率。