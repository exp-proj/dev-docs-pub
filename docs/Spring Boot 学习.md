## Spring Boot

> 参考：https://github.com/ityouknow/spring-boot-leaning

- Spring Boot 是由 Pivotal 团队提供的全新框架，**它的核心设计思想是：约定优于配置，Spring Boot 所有开发细节都是依据此思想进行实现的。**

**Spring Boot 特性**

- 使用 Spring 项目引导页面可以在几秒构建一个项目；
- 方便对外输出各种形式的服务，如 REST API、WebSocket、Web、Streaming、Tasks；
- 非常简洁的安全策略集成；
- 支持关系数据库和非关系数据库；
- 支持运行期内嵌容器，如 Tomcat、Jetty；
- 强大的开发包，支持热启动；
- 自动管理依赖；
- 自带应用监控；
- 支持各种 IDE，如 IntelliJ IDEA、NetBeans。

#### Spring Boot 2.0 都更新了什么

- 第一类，基础环境升级；
- 第二类，默认软件替换和优化；
- 第三类，新技术的引入。

