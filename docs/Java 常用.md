#### Java 常用

- 要将负数转换为正数（这称为绝对值），请使用Math.abs（） 。此`Math.abs()`方法的工作方式如下：“ `number = (number < 0 ? -number : number);` ”。