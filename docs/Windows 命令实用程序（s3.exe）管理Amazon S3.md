## 使用Windows命令实用程序（s3.exe）管理Amazon S3

**s3.exe**是可用于管理S3水桶和EC2环境中独立的Windows命令行实用程序。这个文件没有任何要求安装。要使用这个脚本工作，我们只需要安装.NET Framework 2.0或以上版本。虽然我知道，这个脚本限制选项，但它会为S3存储任务和EBS卷的快照管理对你有帮助。

## 下载s3.exe工具

要下载文件s3.exe访问[s3.codeplex.com](http://s3.codeplex.com/)并下载。下载此文件后，你可以把它放在C:WINDOWSsystem32下，所以我们可以在系统中使用它的路径。 [http://s3.codeplex.com](http://s3.codeplex.com/)

## 设置s3.exe认证

此实用程序提供选项来保存身份验证的将来，获取从AWS securityCredentials页这些安全密钥。它会提示登录到您的Amazon帐户。

```
C:> s3 auth [AWS ACCESS KEY]  [AWS SECRET KEY]
```

## 如何使用s3.exe实用工具

### 1.列出所有桶

以下命令将列出您的S3帐户中的所有时段。

```
s3 list
```

### 2.上传文件到桶

要更新单个或多个文件到S3桶

```
s3 put mybucket/backups/ c:backupfilesmyFile.bak
s3 put mybucket/backups/ c:backupfiles*.bak
```

### 3.上传目录到桶

如果你需要，你也可以上传整个目录到S3桶。

```
s3 put mybucket/backups/ c:backupfiles
```

### 4.同步目录

虽然上传整个目录，如果同一个目录服务器上使用已经存在/sync 到仅同步变化。

```
s3 put mybucket/backups/ c:backupfiles /sync
```

### 5.从桶下载文件

要下载单个从S3桶本地文件系统的多个文件。

```
s3 get mybucket/*.bak
s3 get mybucket/myFile.bak
```

### 6.从桶下载目录

若要从S3桶下载整个目录。下面的命令将下载的备份目录形式mybucket到本地系统的当前工作目录。为了更多地了解这个命令用“s3.exe帮助获得”。

```
s3 get mybucket/backups/ /sub
```







# 命令行管理aws s3

AWS官方文档：

http://docs.amazonaws.cn/cli/latest/userguide/using-s3-commands.html

## 管理存储桶

 创建桶;

```
$ aws s3 mb s3://bucket-name
```

 删除桶：

```
$ aws s3 rb s3://bucket-name
```

删除非空桶：

```
$ aws s3 rb s3://bucket-name --force
```

列出存储桶

```
$ aws s3 ls
```

列出存储桶中所有的对象和文件夹

```
$ aws s3 ls s3://bucket-name
```

列出桶中 *`bucket-name`*/`MyFolder` 中的对象

```
$ aws s3 ls s3://bucket-name/MyFolder
```

## 管理对象

命令包括 `aws s3 cp`、`aws s3 ls`、`aws s3 mv`、`aws s3 rm` 和 `sync`。`cp`、`ls`、`mv` 和 `rm` 命令的用法与它们在 Unix 中的对应命令相同。

```
// 将当前目录里的 MyFile.txt文件拷贝到 s3://my-bucket/MyFolder
$ aws s3 cp MyFile.txt s3://my-bucket/MyFolder/

// 将s3://my-bucket/MyFolder所有 .jpg 的文件移到 ./MyDirectory
$ aws s3 mv s3://my-bucket/MyFolder ./MyDirectory --exclude '*' --include '*.jpg' --recursive

// 列出  my-bucket的所有内容
$ aws s3 ls s3://my-bucket

// 列出my-bucket中MyFolder的所有内容
$ aws s3 ls s3://my-bucket/MyFolder

// 删除 s3://my-bucket/MyFolder/MyFile.txt
$ aws s3 rm s3://my-bucket/MyFolder/MyFile.txt

// 删除 s3://my-bucket/MyFolder 和它的所有内容
$ aws s3 rm s3://my-bucket/MyFolder --recursive
当 --recursive 选项与 cp、mv 或 rm 一起用于目录/文件夹时，命令会遍历目录树，包括所有子目录
```

sync命令

`sync` 命令的形式如下。可能的源-目标组合有：

- 本地文件系统到 Amazon S3
- Amazon S3 到本地文件系统
- Amazon S3 到 Amazon S3

$ aws s3 sync <source> <target> [--options]

例如：本地文件系统到S3中：

```
$ aws s3 sync 本地目录/. s3://my-bucket/目录
```