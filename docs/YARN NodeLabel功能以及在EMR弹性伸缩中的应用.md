## YARN NodeLabel功能以及在EMR弹性伸缩中的应用

> YARN Node Label功能最早是在Hadoop 2.6版本中引入，在后续版本中有更多的功能完善。到了Hadoop 2.8.x版本之后，该功能已经比较完整，可以满足日常使用。

> 其实Node Label特性更准确的叫法是Node Partition，也就是说通过label把YARN集群中的节点分组，每个节点拥有一个label，通过调度器的配置，将作业Task调度到指定的节点中，如果节点没有配置Label，那么这个节点属于Label为DEFAULT的Partition。

> Hadoop 3.2之后加入的Node Attribute功能是更加灵活的方案，可以方便的给各个节点打上OS/kernel version/CPU architecture/JDK version等标签

`Node label is a way to group nodes with similar characteristics and applications can specify where to run`

> 队列资源控制，通过设置每个队列可以使用的标签资源的百分比，实现控制

`yarn.scheduler.capacity.<queue-path>.accessible-node-labels.<label>.capacity `

`Set the percentage of the queue can access to nodes belong to <label> partition . The sum of <label> capacities for direct children under each parent, must be equal to 100. By default, it’s 0.`