# git 中文文件名 乱码

- 原因：

   - git 默认中文文件名是 \xxx\xxx 等八进制形式 是因为 对0x80以上的字符进行quote 

- 解决方式：
    - `git config –global core.quotepath false`
    - 或者 直接在 `.git/config` 文件中`[core]`下添加 `quotepath = false` 即可

    ![git中文乱码配置](./asserts/git中文乱码配置.png)

> core.quotepath设为false的话，就不会对0x80以上的字符进行quote。中文显示正常


