# Java中的Properties类详解

**1. Properties类是什么？**

> Properties（Java.util.Properties），该类主要用于读取Java的配置文件，不同的编程语言有自己所支持的配置文件，配置文件中很多变量是经常改变的，为了方便用户的配置，能让用户够脱离程序本身去修改相关的变量设置。就像在Java中，其配置文件常为.properties文件，是以键值对的形式进行参数配置的。

**2. 常用的方法**

> getProperty([String](https://blog.csdn.net/amosjob/java/lang/String.html) key)  在此属性列表中搜索具有指定键的属性。如果在此属性列表中找不到该键，则会检查默认属性列表及其默认值（递归）。如果未找到该属性，则该方法返回默认值参数。
>
> list([PrintStream](https://blog.csdn.net/amosjob/java/io/PrintStream.html) out) 将此属性列表打印到指定的输出流。此方法对于调试很有用。
>
> load([InputStream](https://blog.csdn.net/amosjob/java/io/InputStream.html) inStream) 从输入字节流中读取属性列表（键和元素对）。输入流采用加载（Reader）中指定的简单的面向行的格式，并假定使用ISO 8859-1字符编码;即每个字节是一个Latin1字符。不在Latin1中的字符和某些特殊字符在使用Unicode转义符的键和元素中表示。 此方法返回后，指定的流仍保持打开状态。
>
> setProperty([String](https://blog.csdn.net/amosjob/java/lang/String.html) key, [String](https://blog.csdn.net/amosjob/java/lang/String.html) value) 调用 Hashtable 的方法 put 。他通过调用基类的put方法来设置 键值对。
>
> store([OutputStream](https://blog.csdn.net/amosjob/java/io/OutputStream.html) out, [String](https://blog.csdn.net/amosjob/java/lang/String.html) comments) 将此Properties表中的此属性列表（键和元素对）以适合使用load（InputStream）方法加载到Properties表的格式写入输出流。 此Properties方法不会写出此Properties表的defaults表中的属性（如果有）。
>
> storeToXML([OutputStream](https://blog.csdn.net/amosjob/java/io/OutputStream.html) os, [String](https://blog.csdn.net/amosjob/java/lang/String.html) comment, [String](https://blog.csdn.net/amosjob/java/lang/String.html) encoding) 使用指定的编码发出表示此表中包含的所有属性的XML文档。
>
> clear() 清除此哈希表，使其不包含任何键。
>
> stringPropertyNames() 返回此属性列表中的一组键，其中键及其对应的值是字符串，如果尚未从主属性列表中找到相同名称的键，则包括默认属性列表中的不同键。键或键不是String类型的属性将被省略。

**3. 读取配置文件**

> class myProperties {
>    public static void main(String[] args) throws Exception {
>      Properties pps = new Properties();
>      pps.load(new FileInputStream("file.properties"));
>      Enumeration fileName = pps.propertyNames();
>      while (fileName.hasMoreElements()) {
>        String strKey = (String) fileName.nextElement();
>        String strValue = pps.getProperty(strKey);
>        System.out.println(strKey + "," + strValue);
>      }
>    }
> }
>
> *创建一个配置文件 file.properties*
>
> *age = 25*
>
> *address = beijing*

使用Properties配置PageHelper插件

```java
@Configuration
public class PageHelperConfig {
   @Bean
   public PageHelper pageHelper(){
       PageHelper pageHelper = new PageHelper();
       Properties properties = new Properties();

       // 设置为true时，会将RowBounds第一个参数offset当成pageNum页码使用
       properties.setProperty("offsetAsPageNum","true");

       // 查询总条数
       properties.setProperty("rowBoundsWithCount","true");
       /**
        * 配置合理分页，如果为turn,pageNum<1会查询第一页，
        * 如果pageNum>pages会查询最后一页,为false则返回空
        */
       properties.setProperty("reasonable","true");

       // 数据库方言
       properties.setProperty("dialect","mysql");   
       pageHelper.setProperties(properties);
       return pageHelper;
   }
}
```

Java API 中文在线

> http://www.matools.com/api/java8